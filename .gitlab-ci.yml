cache: &global_cache
  policy: pull-push
  paths:
    - .gradleHome

variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false -Dorg.gradle.console=plain"
  DOCKER_DRIVER: overlay2
  FF_USE_FASTZIP: "true"
  ARTIFACT_COMPRESSION_LEVEL: fast
  CACHE_COMPRESSION_LEVEL: fast
  TRANSFER_METER_FREQUENCY: 5s

stages:
  - build
  - test1
  - test2
  - package

build:gradle:
  image: gradle:jdk11-alpine
  stage: build
  dependencies: []
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
  script:
    - ./gradlew clean classes testClasses installDist
  artifacts:
    expire_in: 2hrs
    paths:
      - .gradle
      - "**/build/classes/"
      - "**/build/resources/"
      - "**/build/install/"
      - "**/build/libs/"

test:ktlint:
  image: alpine
  stage: test1
  dependencies: []
  cache: {}
  needs: []
  before_script:
    - apk add --no-cache curl openjdk11-jre-headless
    - curl --location --output /usr/local/bin/ktlint https://github.com/pinterest/ktlint/releases/latest/download/ktlint
    - chmod +x /usr/local/bin/ktlint
  allow_failure: true
  script:
    - ktlint

test:hadolint:
  image: hadolint/hadolint:latest-alpine
  stage: test1
  allow_failure: true
  dependencies: []
  needs: []
  cache: {}
  script:
    - hadolint --ignore DL3018 Dockerfile

test:openldap:
  image: gradle:jdk11-alpine
  stage: test1
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    LDAP_HOST: "openldap"
    LDAP_ORGANISATION: "TEST"
    LDAP_DOMAIN: "example.org"
    LDAP_ADMIN_PASSWORD: "admin"
  services:
    - name: osixia/openldap
      alias: openldap
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
  script:
    - ./gradlew :authorization:check
  artifacts:
    when: always
    reports:
      junit:
        - "**/build/test-results/test/**/TEST-*.xml"

test:389ds:
  image: gradle:jdk11-alpine
  stage: test1
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    LDAP_HOST: "389ds"
    LDAP_BIND_USER: "cn=Directory Manager"
    LDAP_BIND_PASSWORD: "Admin123"
    LDAP_UNIQUE_GROUPS: "false"
    LDAP_BASE_DN: "dc=example,dc=com"
  services:
    - name: minkwe/389ds
      alias: 389ds
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
  script:
    - ./gradlew :authorization:check
  artifacts:
    when: always
    reports:
      junit:
        - "**/build/test-results/test/**/TEST-*.xml"

test:nextcloud:
  image: gradle:jdk11-alpine
  stage: test1
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    NEXTCLOUD_ADMIN_USER: "ncadmin" # for Nextcloud
    NEXTCLOUD_ADMIN_PASSWORD: "ncadminsecret"
    NEXTCLOUD_TRUSTED_DOMAINS: "localhost nextcloud"
    SQLITE_DATABASE: nctest # to enable auto installation with SQLite
    NEXTCLOUD_BASE_URL: "http://nextcloud" # for Feather
    NEXTCLOUD_USER: "ncadmin"
    NEXTCLOUD_PWD: "ncadminsecret"
  services:
    - name: nextcloud:22
      alias: nextcloud
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
  script:
    # Wait for Nextcloud to be up, ready and running
    - apk add --no-cache curl bash
    - integration/nextcloud_tests/wait_ready.sh
    - integration/nextcloud_tests/install_nc_apps.sh
    - ./gradlew :nextcloud:check
  artifacts:
    when: always
    reports:
      junit: 
        - "**/build/test-results/test/**/TEST-*.xml"

test:openproject:
  image: gradle:jdk11-alpine
  stage: test1
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_USER: "admin"
    OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_PASSWORD: "admin"
    OPENPROJECT_AUTH__SOURCE__SSO_HEADER: "X-Remote-User"
    OPENPROJECT_AUTH__SOURCE__SSO_SECRET: "s3cr3t"
    OPENPROJECT_AUTH__SOURCE__SSO_OPTIONAL: "true"
    OPENPROJECT_HOST: "http://openproject"
    OPENPROJECT_POSTGRES_HOST: "openproject"
    OPENPROJECT_POSTGRES_PORT: 5432
  services:
    - name: openproject/community:11.4.1
      alias: openproject
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apk add --no-cache postgresql-client curl
  script:
    # Wait for OpenProject to be up, ready and running
    - integration/openproject/wait_ready.sh
    - integration/openproject/migrate_db.sh
    # Do the test
    - ./gradlew :openproject:check
  artifacts:
    when: always
    reports:
      junit: "**/build/test-results/test/**/TEST-*.xml"

test:multiserviceAndIog:
  image: gradle:jdk11-alpine
  stage: test2
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    LDAP_HOST: "389ds"
    LDAP_BIND_USER: "cn=Directory Manager"
    LDAP_BIND_PASSWORD: "Admin123"
    LDAP_UNIQUE_GROUPS: "false"
    LDAP_BASE_DN: "dc=example,dc=com"
    NEXTCLOUD_ADMIN_USER: "ncadmin" # for Nextcloud
    NEXTCLOUD_ADMIN_PASSWORD: "ncadminsecret"
    NEXTCLOUD_TRUSTED_DOMAINS: "localhost nextcloud"
    SQLITE_DATABASE: nctest # to enable auto installation with SQLite
    NEXTCLOUD_BASE_URL: "http://nextcloud" # for Feather
    NEXTCLOUD_USER: "ncadmin"
    NEXTCLOUD_PWD: "ncadminsecret"
    TEST_REDIS_HOST: "redis"
    OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_USER: "admin"
    OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_PASSWORD: "admin"
    OPENPROJECT_AUTH__SOURCE__SSO_HEADER: "X-Remote-User"
    OPENPROJECT_AUTH__SOURCE__SSO_SECRET: "s3cr3t"
    OPENPROJECT_AUTH__SOURCE__SSO_OPTIONAL: "true"
    OPENPROJECT_HOST: "http://openproject"
    OPENPROJECT_WEB_TIMEOUT: 120
    OPENPROJECT_WEB_WAIT__TIMEOUT: 60
    OPENPROJECT_POSTGRES_HOST: "openproject"
    OPENPROJECT_POSTGRES_PORT: 5432
  services:
    - name: minkwe/389ds
      alias: 389ds
    - name: nextcloud:22
      alias: nextcloud
    - name: openproject/community:11.4.1
      alias: openproject
    - name: redis:latest
      alias: redis
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apk add --no-cache postgresql-client curl bash
  script:
    - integration/nextcloud_tests/wait_ready.sh
    - integration/nextcloud_tests/install_nc_apps.sh
    - integration/openproject/wait_ready.sh
    - integration/openproject/migrate_db.sh --iog
    - ./gradlew :core:check :lib:check :multiservice:check :iog:check
  artifacts:
    when: always
    reports:
      junit:
        - "**/build/test-results/test/**/TEST-*.xml"

build:docker:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: package
  cache: {}
  dependencies:
    - build:gradle
  before_script:
    - if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then TAG_LIST="latest"; fi
    - if [[ ! -z "$CI_COMMIT_TAG" ]]; then TAG_LIST="$TAG_LIST stable $CI_COMMIT_TAG"; fi
    - for TAG in $TAG_LIST; do FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CI_REGISTRY_IMAGE:$TAG "; done;
    - FORMATTEDTAGLIST=$(echo "${FORMATTEDTAGLIST}" | sed s/\-\-tag/\-\-destination/g)
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile $FORMATTEDTAGLIST --cache=true --verbosity=warn
