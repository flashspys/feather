/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.mocks

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User

class CredentialMock : ICredentialProvider {
    val users = HashMap<String, User>()
    val groups = HashMap<String, Group>()
    val passwords = HashMap<String, String>()

    override fun getUser(id: String): User? {
        return users.getOrDefault(id, null)
    }

    override fun getUserByMail(mail: String): User? {
        return getUsers().firstOrNull { it.mail == mail }
    }

    override fun getGroupByName(groupname: String): Group? {
        return getGroups().firstOrNull { it.name == groupname }
    }

    override fun getUserByUsername(username: String): User? {
        return getUsers().firstOrNull { it.username == username }
    }

    override fun getUsers(): Collection<User> {
        return users.map { it.value }
    }

    override fun createUser(user: User, password: String?): User {
        val id = users.count().toString() + 1
        var userCopy = user.copy(id = id, groups = (user.groups + user.ownedGroups).distinct().toSet())
        users[id] = userCopy
        passwords[id] = password ?: ""
        return users[id]!!
    }

    override fun deleteUser(user: User) {
        users.remove(user.id)
        passwords.remove(user.id)
    }

    override fun updateUser(user: User): User? {
        users[user.id] = user.copy(groups = (user.groups + user.ownedGroups).distinct().toSet())
        return users[user.id]
    }

    override fun authenticateUser(username: String, password: String): User? {
        val id = users.values.firstOrNull { it.username == username }?.id ?: return null
        if (passwords[id] != password)
            return null
        return users[id]!!
    }

    override fun authenticateUserByMail(mail: String, password: String): User? {
        val id = users.values.firstOrNull { it.mail == mail }?.id ?: return null
        if (passwords[id] != password)
            return null
        return users[id]!!
    }

    override fun updateUserPassword(user: User, password: String): Boolean {
        if (!users.containsKey(user.id))
            return false
        passwords[user.id] = password
        return true
    }

    override fun getGroup(id: String): Group? {
        groups[id] ?: return null
        return groups[id]
    }

    override fun getGroups(): Collection<Group> {
        return groups.map { it.value }
    }

    override fun createGroup(group: Group): Group {
        val newID = (groups.count() + 1).toString()
        groups[newID] = group.copy(
            id = newID, userMembers = (group.userMembers + group.owners).distinct().toSet(),
            groupMembers = group.groupMembers + group.ownerGroups,
            parentGroups = group.parentGroups + group.ownedGroups
        )
        group.parentGroups.forEach {
            if (!groups[it]?.ownedGroups!!.contains(group.id)) groups[it] = groups[it]!!.copy(ownedGroups = groups[it]!!.ownedGroups + newID)
            if (!groups[it]?.groupMembers!!.contains(group.id)) groups[it] = groups[it]!!.copy(groupMembers = groups[it]!!.groupMembers + newID)
        }
        group.userMembers.forEach {
            if (!users[it]?.groups!!.contains(group.id)) users[it] = users[it]!!.copy(groups = users[it]!!.groups + group.id)
        }
        return groups[newID]!!
    }

    override fun deleteGroup(group: Group) {
        groups.remove(group.id)
    }

    // Update a User, the userbackend should retrieve the actual value from the user and update it to the parameter user
    override fun updateGroup(group: Group): Group? {
        groups[group.id] = group.copy(
            userMembers = (group.userMembers + group.owners).distinct().toSet(),
            groupMembers = group.groupMembers + group.ownerGroups,
            parentGroups = group.parentGroups + group.ownedGroups
        )
        group.parentGroups.forEach {
            if (!groups[it]?.ownedGroups!!.contains(group.id)) groups[it] = groups[it]!!.copy(ownedGroups = groups[it]!!.ownedGroups + groups[group.id]!!.id)
            if (!groups[it]?.groupMembers!!.contains(group.id)) groups[it] = groups[it]!!.copy(groupMembers = groups[it]!!.groupMembers + groups[group.id]!!.id)
        }
        groups[group.id]!!.userMembers.forEach {
            if (!users[it]?.groups!!.contains(group.id)) users[it] = users[it]!!.copy(groups = users[it]!!.groups + group.id)
        }
        return groups[group.id]
    }

    override fun getUsersAndGroups(): Pair<Collection<User>, Collection<Group>> {
        return Pair(getUsers(), getGroups())
    }

    override fun close() {
    }

    fun reset() {
        groups.clear()
        passwords.clear()
        users.clear()
    }
}
