package dev.maximilian.feather.authorization

import dev.maximilian.feather.ICredentialAuthorizer
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Session
import java.time.Instant

class CredentialAuthorizer(
    override val id: String,
    private val credentialProvider: ICredentialProvider,
    private val baseUrl: String
) : ICredentialAuthorizer {
    override fun authorize(username: String, password: String): Session? {
        val user = credentialProvider.authenticateUser(username, password)
            ?: credentialProvider.authenticateUserByMail(username, password)
            ?: return null
        return Session(
            id = EMPTY_UUID,
            validUntil = Instant.now().plusSeconds(600),
            user = user,
            authorizer = this
        )
    }

    override fun refresh(session: Session): Session? = session.takeIf { it.validUntil >= Instant.now() }?.copy(validUntil = Instant.now().plusSeconds(600))

    override fun afterLogoutURL(): String = "$baseUrl/login"
}
