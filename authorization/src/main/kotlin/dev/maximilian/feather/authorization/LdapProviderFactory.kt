/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import com.github.mustachejava.DefaultMustacheFactory
import com.github.mustachejava.MustacheFactory
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.authorization.internals.ldap.Ldap
import dev.maximilian.feather.authorization.internals.ldap.SelfSignedCertTrustManager
import dev.maximilian.feather.authorization.internals.validateCertificate
import mu.KLogging
import java.io.StringReader
import java.io.StringWriter
import java.io.Writer
import java.util.UUID

class LdapProviderFactory(propertyMap: MutableMap<String, String>) {
    companion object : KLogging() {
        private val mustacheFactory: MustacheFactory = LdapMustacheFactory()

        private fun validateBoolean(unvalidated: String): Boolean? = when (unvalidated.trim()) {
            "true", "1" -> true
            "false", "0" -> false
            else -> null
        }

        fun executeBaseDnMustache(value: String, baseDn: String) = StringWriter().also {
            mustacheFactory.compile(StringReader(value), UUID.randomUUID().toString()).execute(it, BaseDnMustache(baseDn))
        }.toString()
    }

    // Properties to connect to LDAP
    private val host = propertyMap.getOrPut("ldap.host") { LdapConstants.HOST }
    private val port = propertyMap.getOrPut("ldap.port") { LdapConstants.PORT }
    private val useTls = propertyMap.getOrPut("ldap.tls") { LdapConstants.TLS }
    private val ca = propertyMap.getOrPut("ldap.ca") { LdapConstants.CA }

    // Properties to authenticate against LDAP
    private val bindDn = propertyMap.getOrPut("ldap.bind.dn") { LdapConstants.BIND_DN }
    private val bindPassword = propertyMap.getOrPut("ldap.bind.password") { LdapConstants.BIND_PASSWORD }

    // Properties to identify users and groups
    private val baseDn = propertyMap.getOrPut("ldap.dn.base") { LdapConstants.BASE_DN }
    private val baseDnUsers = propertyMap.getOrPut("ldap.dn.users") { LdapConstants.BASE_DN_USERS }
    private val baseDnGroups = propertyMap.getOrPut("ldap.dn.groups") { LdapConstants.BASE_DN_GROUPS }
    private val baseDnPermissions = propertyMap.getOrPut("ldap.dn.permissions") { LdapConstants.BASE_DN_PERMISSIONS }

    // Properties to comply with different LDAP Objects/Providers
    private val rdnUser = propertyMap.getOrPut("ldap.rdn.user") { LdapConstants.RDN_USER }
    private val rdnGroup = propertyMap.getOrPut("ldap.rdn.group") { LdapConstants.RDN_GROUP }
    private val uniqueGroups = propertyMap.getOrPut("ldap.unique_groups") { LdapConstants.UNIQUE_GROUPS }
    private val plainTextPasswords = propertyMap.getOrPut("ldap.plain_text_passwords") { LdapConstants.PLAIN_TEXT_PASSWORDS }

    fun create(): ICredentialProvider {
        // Validate properties to connect to ldap
        val validatedHost = host.trim()
        require(validatedHost.isNotBlank()) { "LDAP host must not be blank" }
        require(port.trim().isNotBlank()) { "LDAP port must not be blank" }
        val validatedPort = port.trim().toIntOrNull()
        require(validatedPort != null) { "LDAP port is not numeric" }
        require(0 < validatedPort && validatedPort < 65536) { "LDAP port is not in range (1 - 65535)" }
        val validatedUseTls = validateBoolean(useTls)
        require(validatedUseTls != null) { "TLS should be enabled (true or 1, recommended) or disabled (false or 0)" }
        val validatedCertificate = validateCertificate(ca)

        // Validate properties to authenticate against ldap
        val validatedBindDn = executeBaseDnMustache(bindDn, baseDn)

        // Validate properties to identify users and groups
        require(baseDn.trim().isNotBlank()) { "LDAP base dn must be not blank" }
        require(baseDnUsers.trim().isNotBlank()) { "LDAP user base dn must be not blank" }
        require(baseDnGroups.trim().isNotBlank()) { "LDAP group base dn must be not blank" }
        require(baseDnPermissions.trim().isNotBlank()) { "LDAP permission base dn must be not blank" }
        val validatedBaseDnUsers = executeBaseDnMustache(baseDnUsers, baseDn)
        val validatedBaseDnGroups = executeBaseDnMustache(baseDnGroups, baseDn)
        val validatedBaseDnPermission = executeBaseDnMustache(baseDnPermissions, baseDn)

        // Validate properties to comply with different LDAP Objects/Providers
        require(rdnUser.trim().isNotBlank()) { "LDAP user rdn type must be not blank" }
        require(rdnGroup.trim().isNotBlank()) { "LDAP group rdn type must be not blank" }
        val validatedUniqueGroups = validateBoolean(uniqueGroups)
        require(validatedUniqueGroups != null) { "Unique groups should be enabled (true or 1, recommended) or disabled (false or 0)" }

        val validatedPlainTextPasswords = validateBoolean(plainTextPasswords)
        require(validatedPlainTextPasswords != null) { "Plain text passwords should be disabled (false or 0, recommended) or enabled (true or 1)" }

        return LdapCredentialProvider(
            Ldap(
                host = validatedHost,
                port = validatedPort,
                useTls = validatedUseTls,
                trustManagers = validatedCertificate?.let { setOf(SelfSignedCertTrustManager(it)) } ?: emptySet(),
                baseDn = baseDn.trim(),
                baseDnGroups = validatedBaseDnGroups,
                baseDnPermissions = validatedBaseDnPermission,
                baseDnUsers = validatedBaseDnUsers,
                bindDn = validatedBindDn.takeIf { it.isNotBlank() },
                bindPassword = bindPassword.takeIf { it.isNotBlank() },
                rdnGroup = rdnGroup.trim(),
                rdnUser = rdnUser.trim(),
                uniqueGroups = validatedUniqueGroups,
                plainTextPasswords = validatedPlainTextPasswords
            )
        )
    }
}

private class LdapMustacheFactory : DefaultMustacheFactory() {
    override fun encode(value: String, writer: Writer) {
        writer.write(value)
    }
}

private data class BaseDnMustache(
    val baseDn: String
)

object LdapConstants {
    const val HOST: String = "127.0.0.1"
    const val PORT: String = "636"
    const val TLS: String = "true"
    const val CA: String = ""

    const val BIND_DN: String = ""
    const val BIND_PASSWORD = ""

    const val BASE_DN: String = ""
    const val BASE_DN_USERS: String = "ou=users,{{ baseDn }}"
    const val BASE_DN_GROUPS: String = "ou=groups,{{ baseDn }}"
    const val BASE_DN_PERMISSIONS: String = "ou=permissions,{{ baseDn }}"

    const val RDN_USER: String = "cn"
    const val RDN_GROUP: String = "cn"
    const val UNIQUE_GROUPS: String = "true"
    const val PLAIN_TEXT_PASSWORDS: String = "false"
}
