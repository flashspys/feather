/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.internals.oidc

import com.nimbusds.jose.crypto.RSASSAVerifier
import com.nimbusds.jose.jwk.RSAKey
import com.nimbusds.jwt.SignedJWT
import com.nimbusds.oauth2.sdk.AuthorizationCode
import com.nimbusds.oauth2.sdk.AuthorizationCodeGrant
import com.nimbusds.oauth2.sdk.RefreshTokenGrant
import com.nimbusds.oauth2.sdk.ResponseType
import com.nimbusds.oauth2.sdk.Scope
import com.nimbusds.oauth2.sdk.TokenErrorResponse
import com.nimbusds.oauth2.sdk.TokenRequest
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic
import com.nimbusds.oauth2.sdk.auth.Secret
import com.nimbusds.oauth2.sdk.id.ClientID
import com.nimbusds.oauth2.sdk.token.BearerAccessToken
import com.nimbusds.oauth2.sdk.token.RefreshToken
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils
import com.nimbusds.openid.connect.sdk.AuthenticationRequest
import com.nimbusds.openid.connect.sdk.LogoutRequest
import com.nimbusds.openid.connect.sdk.OIDCTokenResponse
import com.nimbusds.openid.connect.sdk.OIDCTokenResponseParser
import com.nimbusds.openid.connect.sdk.UserInfoErrorResponse
import com.nimbusds.openid.connect.sdk.UserInfoRequest
import com.nimbusds.openid.connect.sdk.UserInfoResponse
import com.nimbusds.openid.connect.sdk.UserInfoSuccessResponse
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.IOidcAuthorizer
import dev.maximilian.feather.Session
import dev.maximilian.feather.authorization.EMPTY_UUID
import kong.unirest.Unirest
import net.minidev.json.JSONArray
import net.minidev.json.JSONObject
import org.apache.http.client.utils.URIBuilder
import java.net.URI
import java.time.Instant

internal class OidcAutoDiscoveryClient(
    override val id: String,
    override val name: String,
    override val color: String,
    override val iconUrl: String,
    clientId: String,
    clientSecret: String,
    baseURL: String,
    private val callbackUrl: String,
    private val credentialProvider: ICredentialProvider
) : IOidcAuthorizer {
    private val oidcMeta: OIDCProviderMetadata
    private val clientId = ClientID(clientId)
    private val clientSecret = Secret(clientSecret)

    init {
        val discoverUrl = "$baseURL/.well-known/openid-configuration"
        val response = Unirest.get(discoverUrl).asString()
        require(response.status == 200) { "Cannot discover openid configuration at $discoverUrl. Server retuned ${response.status}" }
        oidcMeta = OIDCProviderMetadata.parse(response.body)
    }

    override fun redirectUrl(): String {
        val scope: Scope = Scope.parse("openid")
        val authenticationRequest = AuthenticationRequest(
            oidcMeta.authorizationEndpointURI,
            ResponseType(ResponseType.Value.CODE),
            scope,
            clientId,
            URIBuilder(URI.create(callbackUrl)).addParameter("provider", id).build(),
            null,
            null
        )

        return authenticationRequest.toURI().toString()
    }

    override fun authorize(token: String): Session? {
        val tokenReq = TokenRequest(
            oidcMeta.tokenEndpointURI,
            ClientSecretBasic(
                clientId,
                clientSecret
            ),
            AuthorizationCodeGrant(
                AuthorizationCode(token),
                URIBuilder(URI.create(callbackUrl)).addParameter("provider", name).build()
            )
        )

        val tokenHTTPResp = tokenReq.toHTTPRequest().send()
        val tokenResponse = OIDCTokenResponseParser.parse(tokenHTTPResp)

        return if (tokenResponse is TokenErrorResponse) {
            // Something else is gone wrong
            if (tokenResponse.errorObject.code != "invalid_grant") {
                throw IllegalStateException("Code validation error but not invalid_grant, but: ${tokenResponse.errorObject.code}")
            }

            null
        } else {
            val accessTokenResponse = tokenResponse as OIDCTokenResponse
            verifyIdToken(accessTokenResponse.oidcTokens.idToken as SignedJWT)
            val oidcTokens = accessTokenResponse.oidcTokens
            val accessToken = oidcTokens.accessToken

            val userInfoReq = UserInfoRequest(
                oidcMeta.userInfoEndpointURI,
                accessToken as BearerAccessToken?
            )

            val userInfoHTTPResp = userInfoReq.toHTTPRequest().send()
            val userInfoResponse = UserInfoResponse.parse(userInfoHTTPResp)

            if (userInfoResponse is UserInfoErrorResponse) {
                if (userInfoResponse.errorObject.code != "invalid_grant") {
                    throw IllegalStateException("Code validation error but not invalid_grant, but: ${userInfoResponse.errorObject.code}")
                }

                return null
            }

            val successResponse = userInfoResponse as UserInfoSuccessResponse
            val uuid = successResponse.userInfo.getClaim("uuid")?.toString() ?: throw IllegalStateException(
                "User data retrieved from OIDC provider does not contain uuid claim"
            )

            val user = credentialProvider.getUser(uuid) ?: throw IllegalStateException(
                "User data retrieved from OIDC provider (\"$uuid\") not found in credential provider"
            )

            Session(
                id = EMPTY_UUID,
                user = user,
                refreshToken = oidcTokens.refreshToken.value,
                accessToken = oidcTokens.accessToken.value,
                validUntil = Instant.now().plusSeconds(accessToken.lifetime),
                authorizer = this
            )
        }
    }

    override fun refresh(session: Session): Session? {
        if (session.refreshToken == null) return null
        if (session.validUntil >= Instant.now()) return session

        val tokenReq = TokenRequest(
            oidcMeta.tokenEndpointURI,
            ClientSecretBasic(
                clientId,
                clientSecret
            ),
            RefreshTokenGrant(RefreshToken(session.refreshToken))
        )

        val tokenHTTPResp = tokenReq.toHTTPRequest().send()
        val tokenResponse = OIDCTokenResponseParser.parse(tokenHTTPResp)

        return if (tokenResponse is TokenErrorResponse) {
            // Something else is gone wrong
            if (tokenResponse.errorObject.code != "invalid_grant") {
                throw IllegalStateException("Code validation error but not invalid_grant, but: ${tokenResponse.errorObject.code}")
            }

            null
        } else {
            val accessTokenResponse = tokenResponse as OIDCTokenResponse
            verifyIdToken(accessTokenResponse.oidcTokens.idToken as SignedJWT)

            session.copy(
                validUntil = Instant.now().plusSeconds(accessTokenResponse.oidcTokens.accessToken.lifetime),
                refreshToken = accessTokenResponse.oidcTokens.refreshToken.value,
                accessToken = accessTokenResponse.oidcTokens.accessToken.value
            )
        }
    }

    private fun verifyIdToken(idToken: SignedJWT) {
        val test = Unirest.get(oidcMeta.jwkSetURI.toString()).asString()
        val publicKey = (JSONObjectUtils.parse(test.body)["keys"] as JSONArray).map { it as JSONObject }.firstOrNull {
            it["use"] == "sig" && it["kty"] == "RSA"
        }?.let { RSAKey.parse(it).toRSAPublicKey() }
        idToken.verify(RSASSAVerifier(publicKey))
    }

    override fun afterLogoutURL(): String = LogoutRequest(
        oidcMeta.endSessionEndpointURI,
        null,
        URI.create(callbackUrl),
        null
    ).toURI().toString()
}
