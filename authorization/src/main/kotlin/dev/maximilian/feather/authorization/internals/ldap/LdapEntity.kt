/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.internals.ldap

import dev.maximilian.feather.authorization.ILdapProvider
import java.time.Instant

internal sealed class LdapEntity<T : LdapEntity<T>> {
    abstract val dn: String
    abstract val entryUUID: String
    abstract val rdn: String

    open fun className(ldap: ILdapProvider): String? = this::class.simpleName
}

internal data class InetOrgPerson(
    override val dn: String,
    override val entryUUID: String,
    override val rdn: String,
    val sn: String,
    val mail: Set<String>? = null,
    val givenName: String? = null,
    val displayName: String? = null,
    val employeeNumber: String? = null,
    val userPassword: String? = null,
    val createTimestamp: Instant = Instant.ofEpochSecond(0),
    val cn: String? = null
) : LdapEntity<InetOrgPerson>()

internal data class OrganizationalUnit(
    override val dn: String,
    override val entryUUID: String,
    override val rdn: String
) : LdapEntity<InetOrgPerson>()

internal data class GroupOfNames(
    override val dn: String,
    override val entryUUID: String,
    override val rdn: String,
    val member: Set<String>,
    val owner: Set<String>,
    val description: String?
) : LdapEntity<GroupOfNames>() {
    override fun className(ldap: ILdapProvider): String = if (ldap.uniqueGroups) "groupOfUniqueNames" else "groupOfNames"
}
