/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.util

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User

internal class CredentialProfiler(private val benchmark: Benchmark, private val credentialProfiler: ICredentialProvider) : ICredentialProvider {
    override fun authenticateUser(username: String, password: String): User? {
        val uuid = benchmark.start()
        val q = credentialProfiler.authenticateUser(username, password)
        benchmark.stop(uuid, "CredentialProfiler::authenticateUser")
        return q
    }

    override fun authenticateUserByMail(mail: String, password: String): User? {
        val uuid = benchmark.start()
        val q = credentialProfiler.authenticateUserByMail(mail, password)
        benchmark.stop(uuid, "CredentialProfiler::authenticateUserByMail")
        return q
    }

    override fun createGroup(group: Group): Group {
        val uuid = benchmark.start()
        val q = credentialProfiler.createGroup(group)
        benchmark.stop(uuid, "CredentialProfiler::createGroup")
        return q
    }

    override fun close() {
        val uuid = benchmark.start()
        credentialProfiler.close()
        benchmark.stop(uuid, "CredentialProfiler::close")
    }

    override fun createUser(user: User, password: String?): User {
        val uuid = benchmark.start()
        val q = credentialProfiler.createUser(user, password)
        benchmark.stop(uuid, "CredentialProfiler::createUser")
        return q
    }

    override fun deleteGroup(group: Group) {
        val uuid = benchmark.start()
        credentialProfiler.deleteGroup(group)
        benchmark.stop(uuid, "CredentialProfiler::deleteGroup")
    }

    override fun deleteUser(user: User) {
        val uuid = benchmark.start()
        credentialProfiler.deleteUser(user)
        benchmark.stop(uuid, "CredentialProfiler::deleteUser")
    }

    override fun getGroup(id: String): Group? {
        val uuid = benchmark.start()
        val q = credentialProfiler.getGroup(id)
        benchmark.stop(uuid, "CredentialProfiler::getGroup")
        return q
    }

    override fun getGroupByName(groupname: String): Group? {
        val uuid = benchmark.start()
        val q = credentialProfiler.getGroupByName(groupname)
        benchmark.stop(uuid, "CredentialProfiler::getGroupByName")
        return q
    }

    override fun getGroups(): Collection<Group> {
        val uuid = benchmark.start()
        val q = credentialProfiler.getGroups()
        benchmark.stop(uuid, "CredentialProfiler::getGroups")
        return q
    }

    override fun getUser(id: String): User? {
        val uuid = benchmark.start()
        val q = credentialProfiler.getUser(id)
        benchmark.stop(uuid, "CredentialProfiler::getUser")
        return q
    }

    override fun getUserByMail(mail: String): User? {
        val uuid = benchmark.start()
        val q = credentialProfiler.getUserByMail(mail)
        benchmark.stop(uuid, "CredentialProfiler::getUserByMail")
        return q
    }

    override fun getUserByUsername(username: String): User? {
        val uuid = benchmark.start()
        val q = credentialProfiler.getUserByUsername(username)
        benchmark.stop(uuid, "CredentialProfiler::getUserByUsername")
        return q
    }

    override fun getUsers(): Collection<User> {
        val uuid = benchmark.start()
        val q = credentialProfiler.getUsers()
        benchmark.stop(uuid, "CredentialProfiler::getUsers")
        return q
    }

    override fun getUsersAndGroups(): Pair<Collection<User>, Collection<Group>> {
        val uuid = benchmark.start()
        val q = credentialProfiler.getUsersAndGroups()
        benchmark.stop(uuid, "CredentialProfiler::getUsersAndGroups")
        return q
    }

    override fun updateGroup(group: Group): Group? {
        val uuid = benchmark.start()
        val q = credentialProfiler.updateGroup(group)
        benchmark.stop(uuid, "CredentialProfiler::updateGroup")
        return q
    }

    override fun updateUser(user: User): User? {
        val uuid = benchmark.start()
        val q = credentialProfiler.updateUser(user)
        benchmark.stop(uuid, "CredentialProfiler::updateUser")
        return q
    }

    override fun updateUserPassword(user: User, password: String): Boolean {
        val uuid = benchmark.start()
        val q = credentialProfiler.updateUserPassword(user, password)
        benchmark.stop(uuid, "CredentialProfiler::updateUserPassword")
        return q
    }
}
