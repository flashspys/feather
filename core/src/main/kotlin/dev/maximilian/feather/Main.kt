/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.api.ActionApi
import dev.maximilian.feather.api.ConfigApi
import dev.maximilian.feather.api.GdprAPI
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.authorization.AuthorizerProviderFactory
import dev.maximilian.feather.authorization.CachedCredentialProvider
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.authorization.LdapProviderFactory
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.database.DatabaseProperties
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.iog.IogPluginFactory
import dev.maximilian.feather.iog.ServiceFactory
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.api.BackgroundJobApi
import dev.maximilian.feather.multiservice.events.UserCreationEvent
import dev.maximilian.feather.multiservice.events.UserDeletionEvent
import dev.maximilian.feather.plugin.FeatherPluginInitialization
import dev.maximilian.feather.plugin.PluginLoader
import dev.maximilian.feather.util.Benchmark
import dev.maximilian.feather.util.CredentialProfiler
import dev.maximilian.feather.util.FeatherProperties
import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import redis.clients.jedis.JedisPool
import java.sql.DriverManager
import java.time.Instant
import java.util.Properties
import java.util.UUID

internal fun getEnv(name: String, default: String? = null): String =
    System.getenv(name) ?: default ?: throw IllegalArgumentException("Environment variable $name not found")

public fun main() {
    System.setProperty("loglevel", getEnv("LOGLEVEL", "INFO"))
    val logger = KotlinLogging.logger { }
    logger.info { "Starting Feather Backend version: ${Main.VERSION}" }
    FeatherProperties.apply { /* Init all database values! */ }

    val additionalAllowedOrigins =
        jacksonObjectMapper().readValue<Set<String>>(FeatherProperties.ADDITIONAL_ALLOWED_ORIGINS)

    if (Main.CREDENTIAL_MANAGER.getUsers().none { it.permissions.contains(Permission.ADMIN) }) {
        val password = UUID.randomUUID().toString()
        logger.warn {
            """
                ########## NO ADMIN USER FOUND


                No admin user found, creating "admin <admin@example.org>" with password "$password"


                ########## CREATED DEFAULT ADMIN
            """.trimIndent()
        }
        Main.CREDENTIAL_MANAGER.createUser(
            User(
                id = "",
                username = "admin",
                displayName = "Feather Admin",
                firstname = "Admin",
                surname = "Admin",
                mail = "admin@example.org",
                groups = emptySet(),
                registeredSince = Instant.now(),
                ownedGroups = emptySet(),
                permissions = setOf(Permission.ADMIN),
                disabled = false
            ),
            password
        )
    }

    val appBuilder = AppBuilder()
    val app = appBuilder.createApp(Main.VERSION, "feather", FeatherProperties.BASE_URL, additionalAllowedOrigins)
    val pluginLoader = PluginLoader()

    runCatching {
        val actionController = ActionController(Main.DATABASE)
        val gdprController = GdprController(Main.DATABASE, actionController, Main.CREDENTIAL_MANAGER::getUsers)

        val authorizationController = AuthorizationController(
            Main.AUTHORIZER_SET,
            Main.CREDENTIAL_MANAGER::getUser,
            Main.DATABASE,
            actionController
        )

        val backgroundJobManager = BackgroundJobManager(Main.REDIS)
        BackgroundJobApi(app, backgroundJobManager)

        pluginLoader.initialize(
            FeatherPluginInitialization(
                propertyMap = Main.PROPERTIES,
                baseUrl = FeatherProperties.BASE_URL,
                db = Main.DATABASE,
                app = app,
                credentialProvider = Main.CREDENTIAL_MANAGER,
                actionController = actionController,
                authorizationController = authorizationController
            )
        )
        pluginLoader.start()

        val authorizerApi =
            AuthorizerApi(app, authorizationController, FeatherProperties.DISABLE_SECURE_COOKIE == "true")
        ActionApi(app, actionController, authorizationController)
        GdprAPI(app, gdprController) { Main.CREDENTIAL_MANAGER.getUsers().size }

        val iogPlugin = IogPluginFactory.create(
            backgroundJobManager, authorizerApi as ISessionOperations,
            listOf(object :
                    UserDeletionEvent {
                    override fun userDeleted(user: User) {
                        gdprController.handleUserDeleted(user)
                    }
                }),
            listOf(object :
                    UserCreationEvent {
                    override fun userCreated(user: User) {
                        gdprController.handleUserCreated(user)
                    }
                })
        )

        val multiservice = ServiceFactory(app).createMultiService(
            authorizerApi,
            iogPlugin.synchronizeEvent,
            iogPlugin.userDeletionEvents,
            iogPlugin.userCreationEvents,
            backgroundJobManager,
            gdprController,
            iogPlugin.checkUserEvent,
        )

        iogPlugin.startApis(app, multiservice.services)

        ConfigApi(app, Main.AUTHORIZER_SET, Main.VERSION, iogPlugin.config.openShiftEnabled)
    }.onFailure {
        logger.error("Error while initialising", it)
        pluginLoader.stop()
        app.stop()
        pluginLoader.dispose()
    }
}

internal object Main {
    private val DB_URL by lazy { getEnv("DATABASE_URL") }
    val DATABASE: Database by lazy { Database.Companion.connect(getNewConnection = { DriverManager.getConnection(DB_URL) }) }
    val PROPERTIES: DatabaseProperties by lazy { DatabaseProperties(DATABASE) }
    val BENCHMARK: Benchmark by lazy { Benchmark() }
    val UNCACHED_CREDENTIAL_MANAGER: ICredentialProvider by lazy { LdapProviderFactory(PROPERTIES).create() }
    private val CREDENTIAL_MANAGER_LAZY =
        lazy { CachedCredentialProvider(REDIS, CredentialProfiler(BENCHMARK, UNCACHED_CREDENTIAL_MANAGER)) }
    val CREDENTIAL_MANAGER: ICredentialProvider by CREDENTIAL_MANAGER_LAZY
    val AUTHORIZER_SET: MutableSet<IAuthorizer> by lazy {
        AuthorizerProviderFactory(PROPERTIES).create(
            FeatherProperties.BASE_URL,
            CREDENTIAL_MANAGER
        ).toMutableSet()
    }
    val REDIS: JedisPool by lazy {
        kotlin.runCatching {
            RedisFactory(PROPERTIES).create().apply {
                resource.use { it.ping() }
            }
        }.onFailure {
            if (CREDENTIAL_MANAGER_LAZY.isInitialized()) {
                CREDENTIAL_MANAGER.close()
            }
        }.getOrThrow()
    }
    val VERSION: String by lazy {
        Properties().apply { load(Main::class.java.getResourceAsStream("/application.properties")) }
            .getProperty("version") ?: "undefined"
    }
}
