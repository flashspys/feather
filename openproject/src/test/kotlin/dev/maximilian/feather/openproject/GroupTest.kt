/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.openproject.api.IOpenProjectGroupApi
import dev.maximilian.feather.openproject.api.IOpenProjectUserApi
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.assertDoesNotThrow
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class GroupTest {
    private val api: IOpenProjectGroupApi = TestUtil.openProject
    private val userApi: IOpenProjectUserApi = TestUtil.openProject

    @Test
    fun `getAllGroups() is superset of expected subset`() {
        val expected = (0 until 3).map { runBlocking { api.createGroup(generateTestGroup()) } }.toSet()
        val actual = runBlocking { api.getGroups() }.toSet()
        val setMinus = expected - actual

        assertEquals(emptySet(), setMinus)
    }

    @Test
    fun `getGroup() does return null on not existing id`() {
        assertDoesNotThrow {
            assertNull(runBlocking { api.getGroup(-42) }, "getGroup(NOT_EXISTENT_ID) should return null")
        }
    }

    @Test
    fun `getGroup() returns existing group`() {
        val expected = runBlocking { api.createGroup(generateTestGroup()) }
        val actual = runBlocking { api.getGroup(expected.id) }

        assertNotNull(actual)
        assertEquals(expected, actual)
    }

    @Test
    fun `getGroupByName() works`() {
        val expected = runBlocking { api.createGroup(generateTestGroup()) }
        val actual = runBlocking { api.getGroupByName(expected.name) }

        assertNotNull(actual)
        assertEquals(expected, actual)
    }

    @Test
    fun `createGroup() works`() {
        val group = generateTestGroup()
        val created = runBlocking { api.createGroup(group) }

        assertEquals(group, created.copy(id = 0))
    }

    @Test
    fun `Create already existing group fails with correct message`() {
        val group = generateTestGroup()
        runBlocking { api.createGroup(group) }

        val expectedException = assertFailsWith<IllegalArgumentException> {
            runBlocking { api.createGroup(group) }
        }

        assertEquals("Name has already been taken.", expectedException.message)
    }

    @Test
    fun `deleteGroup() works`() {
        runBlocking {
            val group = api.createGroup(generateTestGroup())

            api.deleteGroup(group)

            require(api.getGroup(group.id) == null) { "Group was not deleted successfully" }
        }
    }

    @Test
    fun `addGroupMember() works`() {
        runBlocking {
            val group = api.createGroup(generateTestGroup())
            val user = userApi.createUser(generateTestUser(), "1234567890")

            api.addGroupMember(group, user)

            assertEquals(listOf(user).map { it.id }, api.getGroup(group.id)!!.members.map { it.id })
        }
    }

    @Test
    fun `removeGroupMember() works`() {
        runBlocking {
            val group = api.createGroup(generateTestGroup())
            val user = userApi.createUser(generateTestUser(), "1234567890")

            api.addGroupMember(group, user)
            api.removeGroupMember(group, user)

            assertEquals(emptyList(), api.getGroup(group.id)!!.members.map { it.id })
        }
    }

    @Test
    fun `addGroupMember() silently returns success if user already in group`() {
        runBlocking {
            val group = api.createGroup(generateTestGroup())
            val user = userApi.createUser(generateTestUser(), "1234567890")

            api.addGroupMember(group, user)

            assertDoesNotThrow { runBlocking { api.addGroupMember(group, user) } }
        }
    }

    @Test
    fun `removeGroupMember() silently returns success if user not a group member`() {
        runBlocking {
            val group = api.createGroup(generateTestGroup())
            val user = userApi.createUser(generateTestUser(), "1234567890")

            assertDoesNotThrow { runBlocking { api.removeGroupMember(group, user) } }
        }
    }

    // This test requires an openproject enterprise license
    // also some manual intervention, so not automatically tested :(
    /*
    @Test
    fun test() {
        runBlocking {
            api.createLdapGroupSynchronisation(api.getGroup(6)!!, "cn=test,dc=example,dc=org", 1)
        }
    }*/
}
