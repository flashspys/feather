/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

class MembershipTest {
    private val api = TestUtil.openProject

    @Test
    fun `createMembership() with user works`() {
        runBlocking {
            val user = api.createUser(generateTestUser(), "1234567890")
            val project = api.createProject(generateTestProject())
            val role = api.getRoles().first { it.name == "Project admin" }

            val membership = OpenProjectMembership(
                0,
                IdLazyEntity(project.id) { project },
                OpenProjectPrincipalLazyEntity(user.id, OpenProjectPrincipalType.USER) { user },
                listOf(role)
            )

            val answer = api.createMembership(membership)

            // The laziness was not touched!
            assertFalse(answer.project.isInitialized())
            assertFalse(answer.principal.isInitialized())

            // Now load and check quality
            assertEquals(membership, answer.copy(id = 0))
        }
    }

    @Test
    fun `createMembership() with group works`() {
        runBlocking {
            val group = api.createGroup(generateTestGroup())
            val project = api.createProject(generateTestProject())
            val role = api.getRoles().first { it.name == "Project admin" }

            val membership = OpenProjectMembership(
                0,
                IdLazyEntity(project.id) { project },
                OpenProjectPrincipalLazyEntity(group.id, OpenProjectPrincipalType.GROUP) { group },
                listOf(role)
            )

            val answer = api.createMembership(membership)

            // The laziness was not touched!
            assertFalse(answer.project.isInitialized())
            assertFalse(answer.principal.isInitialized())

            // Now load and check quality
            assertEquals(membership, answer.copy(id = 0))
        }
    }

    @Test
    fun `getMembership() works`() {
        runBlocking {
            val group = api.createGroup(generateTestGroup())
            val project = api.createProject(generateTestProject())
            val roles = api.getRoles()
            val role = roles.first { it.name == "Project admin" }
            val membership = api.createMembership(OpenProjectMembership(0, project, group, listOf(role)))
            val answer = api.getMembership(membership.id, roles)

            assertNotNull(answer)

            // The answer contains lazy objects
            assertFalse(answer.project.isInitialized())
            assertFalse(answer.principal.isInitialized())

            // Now load and check quality
            assertEquals(membership, answer)
        }
    }

    @Test
    fun `getMemberships() works`() {
        runBlocking {
            val user = api.createUser(generateTestUser(), "1234567890")
            val group = api.createGroup(generateTestGroup())
            val project = api.createProject(generateTestProject())
            val roles = api.getRoles()
            val membership1 = api.createMembership(
                OpenProjectMembership(
                    0,
                    project,
                    group,
                    roles.filter { it.name == "Project admin" }
                )
            )
            val membership2 =
                api.createMembership(OpenProjectMembership(0, project, user, roles.filter { it.name == "Member" }))

            val answer1 = api.getMembership(membership1.id, roles)
            val answer2 = api.getMembership(membership2.id, roles)

            assertNotNull(answer1)
            assertNotNull(answer2)

            // Now load and check quality
            assertEquals(membership1, answer1)
            assertEquals(membership2, answer2)
        }
    }

    @Test
    fun `deleteMembership() works`() {
        runBlocking {
            val group = api.createGroup(generateTestGroup())
            val project = api.createProject(generateTestProject())
            val roles = api.getRoles()
            val role = roles.first { it.name == "Project admin" }

            val answer = api.createMembership(
                OpenProjectMembership(
                    0,
                    IdLazyEntity(project.id) { project },
                    OpenProjectPrincipalLazyEntity(group.id, OpenProjectPrincipalType.GROUP) { group },
                    listOf(role)
                )
            )

            api.deleteMembership(answer)

            // Now load and check quality
            assertNull(api.getMembership(answer.id, roles))
        }
    }

    @Test
    fun `lazy loaded identities with the same id are the same object in getMemberships()`() {
        runBlocking {
            val group1 = api.createGroup(generateTestGroup())
            val group2 = api.createGroup(generateTestGroup())

            val project1 = api.createProject(generateTestProject())
            val project2 = api.createProject(generateTestProject())

            val roles = api.getRoles()
            val role = roles.first { it.name == "Project admin" }

            val m1 = api.createMembership(
                OpenProjectMembership(
                    0,
                    project1,
                    group1,
                    listOf(role)
                )
            )

            val m2 = api.createMembership(
                OpenProjectMembership(
                    0,
                    project2,
                    group1,
                    listOf(role)
                )
            )

            val m3 = api.createMembership(
                OpenProjectMembership(
                    0,
                    project2,
                    group2,
                    listOf(role)
                )
            )

            val memberships = api.getMemberships(roles).filter { it.id == m1.id || it.id == m2.id }

            assertEquals(2, memberships.size)
            assertTrue("Principals must be the same object") { memberships[0].principal === memberships[1].principal }

            val memberships2 = api.getMemberships(roles).filter { it.id == m2.id || it.id == m3.id }

            assertEquals(2, memberships2.size)
            assertTrue("Projects must be the same object") { memberships2[0].project === memberships2[1].project }
        }
    }
}
