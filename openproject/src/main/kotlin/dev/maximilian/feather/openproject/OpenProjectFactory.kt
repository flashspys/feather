/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openproject

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.features.auth.Auth
import io.ktor.client.features.auth.providers.BasicAuthCredentials
import io.ktor.client.features.auth.providers.basic
import io.ktor.client.features.defaultRequest
import io.ktor.client.features.json.Json
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.accept
import io.ktor.client.request.header
import io.ktor.http.ContentType
import io.ktor.http.contentType
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.net.URL

public class OpenProjectFactory {
    private companion object : KLogging()
    private val debug = System.getenv("DEBUG") == "true"

    public fun create(
        baseUrl: String,
        authUser: String,
        authPassword: String,
        ssoHeader: String,
        ssoAdmin: String,
        ssoSecret: String,
        requestRetryCount: Int = 0
    ): IOpenProject {
        // Check baseUrl
        val validatedURL = baseUrl.trim().removeSuffix("/").takeIf { it.isNotBlank() }
            ?: throw IllegalArgumentException("OpenProject::create OpenProject baseUrl not configured")

        kotlin.runCatching { URL(validatedURL) }.onFailure {
            throw IllegalArgumentException("OpenProject::create OpenProject base url malformed", it)
        }

        // Check api user and password
        val validatedAuthUser = authUser.trim()
        require(validatedAuthUser.isNotBlank()) { "OpenProject::create OpenProject api key must not be empty" }

        val validatedAuthPassword = authPassword.trim()
        require(validatedAuthPassword.isNotBlank()) { "OpenProject::create OpenProject api key must not be empty" }

        // Check sso header, user and secret
        val validatedSsoHeader = ssoHeader.trim()
        require(validatedSsoHeader.isNotBlank()) { "OpenProject::create OpenProject sso http header field name must not be empty" }

        val validatedSsoAdmin = ssoAdmin.trim()
        require(validatedSsoAdmin.isNotBlank()) { "OpenProject::create OpenProject sso admin username must not be empty" }

        val validatedSsoSecret = ssoSecret.trim()
        require(validatedSsoSecret.isNotBlank()) { "OpenProject::create OpenProject admin sso secret must not be empty" }

        kotlin.runCatching { URL(validatedURL) }
            .onFailure {
                throw IllegalArgumentException("OpenProject::create OpenProject base url malformed", it)
            }

        val openProject = OpenProject(
            apiClient = createApiHttpClient(validatedAuthUser, validatedAuthPassword, requestRetryCount),
            webpageClient = createWebpageHttpClient(validatedSsoHeader, validatedSsoAdmin, validatedSsoSecret, requestRetryCount),
            baseUrl = validatedURL
        )

        return kotlin.runCatching {
            // Check if the api user has admin permissions
            require(runBlocking { openProject.getMe().admin }) { "To use the OpenProject API functionality a valid user with admin permissions must be configured" }

            val ssoUser = runBlocking { openProject.getUserByLogin(ssoAdmin) }

            require(ssoUser != null) { "To use the OpenProject API functionality a valid sso user must be configured (user not found)" }

            require(ssoUser.admin) { "To use the OpenProject API functionality a valid sso user with admin permissions must be configured (user is not admin)" }

            require(ssoUser.authSource != 0) { "To use the OpenProject API functionality a valid sso user must be configured (auth source is 0)" }

            logger.info { "OpenProject binding - enabled" }
            logger.info { "OpenProject binding - Api User: $validatedAuthUser with password/key length: ${validatedAuthPassword.length}" }
            logger.info { "OpenProject binding - SSO User: $validatedSsoAdmin with header \"$validatedSsoHeader\" and key length: ${validatedSsoSecret.length}" }

            openProject
        }.onFailure { openProject.close() }.getOrThrow()
    }

    private fun HttpClientConfig<*>.configureLogging() = Logging {
        logger = object : Logger {
            override fun log(message: String) {
                OpenProjectFactory.logger.info { message }
            }
        }
        level = if (debug) LogLevel.ALL else LogLevel.INFO
    }

    private fun HttpClientConfig<*>.configureJson() = Json {
        serializer = KotlinxSerializer(
            kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
            }
        )
    }

    private fun createApiHttpClient(authUser: String, authPassword: String, requestRetryCount: Int): HttpClient = HttpClient {
        configureLogging()
        configureJson()

        retryOnTemporarilyServerError()

        defaultRequest {
            contentType(ContentType.Application.Json)
        }

        retryOnTemporarilyServerError { retryCount = requestRetryCount }

        Auth {
            basic {
                sendWithoutRequest { true }
                credentials {
                    BasicAuthCredentials(authUser, authPassword)
                }
            }
        }
    }

    private fun createWebpageHttpClient(ssoHeader: String, ssoAdmin: String, ssoSecret: String, requestRetryCount: Int): HttpClient =
        HttpClient {
            configureLogging()
            configureJson()

            defaultRequest {
                header(ssoHeader, "$ssoAdmin:$ssoSecret")
                accept(ContentType.Text.Html)
            }

            retryOnTemporarilyServerError { retryCount = requestRetryCount }
        }
}
