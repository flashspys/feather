/*
 *
 *  *    Copyright [2021] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud

import dev.maximilian.feather.nextcloud.groupfolders.GroupFoldersAPI
import dev.maximilian.feather.nextcloud.groupfolders.GroupFoldersGroupsAPI
import dev.maximilian.feather.nextcloud.groupfolders.entities.GroupFolderDetails
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class GroupFoldersAPITest {
    companion object {
        private val groupFoldersAPI = GroupFoldersAPI(TestUtil.baseUrl, TestUtil.rest)
    }

    @Test
    fun `Test getAllGroupfolders`() {
        var groupList = groupFoldersAPI.getAllGroupFolders()
        checkNotNull(groupList)
        if (groupList.isEmpty()) {
            val parentFolder = generateTestPath().second
            val newId = groupFoldersAPI.createGroupFolder(parentFolder)
            checkNotNull(newId)

            groupList = groupFoldersAPI.getAllGroupFolders()
            checkNotNull(groupList)

            check(groupList.containsKey(newId.toString()))
            val groupFolderData: GroupFolderDetails? = groupList.get(newId.toString())
            checkNotNull(groupFolderData)
            assertEquals(groupFolderData.mount_point, parentFolder)
        }
        check(!groupList.isEmpty())
    }

    @Test
    fun `Test createGroupFolder`() {
        val parentFolder = generateTestPath().second
        val newId = groupFoldersAPI.createGroupFolder(parentFolder)
        checkNotNull(newId)

        val groupList = groupFoldersAPI.getAllGroupFolders()
        checkNotNull(groupList)
        check(!groupList.isEmpty())
        check(groupList.containsKey(newId.toString()))
    }

    @Test
    fun `Test deleteGroupFolder`() {
        val parentFolder = generateTestPath().second
        val newId = groupFoldersAPI.createGroupFolder(parentFolder)
        checkNotNull(newId)

        val groupList = groupFoldersAPI.getAllGroupFolders()
        checkNotNull(groupList)
        check(!groupList.isEmpty())
        check(groupList.containsKey(newId.toString()))

        groupFoldersAPI.deleteGroupFolder(newId)

        val groupList2 = groupFoldersAPI.getAllGroupFolders()
        checkNotNull(groupList2)
        check(!groupList2.containsKey(newId.toString()))
    }

    @Test
    fun `Test changing properties of groupfolders`() {
        val parentFolder = generateTestPath().second
        val newId = groupFoldersAPI.createGroupFolder(parentFolder)
        checkNotNull(newId)

        check(groupFoldersAPI.changeQuota(newId, "1024"))

        val newPath = generateTestPath().second
        check(groupFoldersAPI.changeMointpoint(newId, newPath))

        val groupList = groupFoldersAPI.getAllGroupFolders()
        checkNotNull(groupList)

        check(groupList.containsKey(newId.toString()))
        val groupFolderData: GroupFolderDetails? = groupList.get(newId.toString())
        checkNotNull(groupFolderData)
        assertEquals(groupFolderData.id, newId)
        assertEquals(groupFolderData.mount_point, newPath)
        assertEquals(groupFolderData.quota, "1024")
    }

    @Test
    fun `Test ACL properties of groupfolders`() {
        val parentFolder = generateTestPath().second
        val newId = groupFoldersAPI.createGroupFolder(parentFolder)
        checkNotNull(newId)

        val groupFoldersGroupsAPI = GroupFoldersGroupsAPI(TestUtil.baseUrl, newId, TestUtil.rest)

        groupFoldersGroupsAPI.addGroup("admin")
        groupFoldersGroupsAPI.changePermissions("admin", PermissionType.readWriteNoDelete.intValue)

        check(groupFoldersAPI.changeAclFlag(newId, true))
        check(groupFoldersAPI.changeAclManager(newId, "group", "admin", true))

        val groupList = groupFoldersAPI.getAllGroupFolders()
        checkNotNull(groupList)
        check(!groupList.isEmpty())
        check(groupList.containsKey(newId.toString()))

        val result = groupList.get(newId.toString())
        checkNotNull(result)
        assertEquals(result.id, newId)
        assertEquals(result.mount_point, parentFolder)
        checkNotNull(result.groups)

        val groups: Map<String, Int> = result.groups!!
        check(!groups.isEmpty())
        checkNotNull(groups.get("admin"))
        check(groups.get("admin") == PermissionType.readWriteNoDelete.intValue)
        check(result.acl)

        val manage = result.manage
        checkNotNull(manage)
        check(!manage.isEmpty())
        checkNotNull(manage.any { it.type == "group" && it.id == "admin" })
    }
}
