/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.asObject
import dev.maximilian.feather.nextcloud.ocs.entities.GetAllUserResponseEntity
import dev.maximilian.feather.nextcloud.ocs.entities.GetUserResponseEntity
import dev.maximilian.feather.nextcloud.ocs.entities.general.ocs
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.MetaEntity
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.NextcloudUserDataEntity
import kong.unirest.HttpResponse
import kong.unirest.UnirestInstance
import java.net.HttpURLConnection
import java.net.URLEncoder

internal data class AddUserResponseSubEntity(
    val id: String
)

internal data class AddUserResponseEntity(
    val meta: MetaEntity,
    val data: AddUserResponseSubEntity
)

internal data class ChangeUserResponseEntity(
    val meta: MetaEntity,
    val data: Array<String> // will always be an empty array
)

internal class UserAPI(baseURL: String, private val restAPI: UnirestInstance) : IUserAPI {
    private val basePath = "ocs/v1.php/cloud"
    private val userPath = "$baseURL/$basePath/users"

    override fun createUser(
        userid: String,
        password: String?, // can be empty to send invitation mail
        displayName: String,
        email: String?, // must not be empty if password is empty
        groups: List<String>,
        subadmin: List<String>,
        quota: String,
        language: String
    ): Boolean {
        if (password == null && email == null)
            throw IllegalArgumentException("Either email or password must be set")
        var body = "userid=$userid"
        if (password != null) {
            body = "$body&password=${encodeString(password)}"
        }
        body = "$body&displayName=$displayName"
        if (email != null) {
            body = "$body&email=${encodeString(email)}"
        }
        groups.forEach { body = "$body&group[]=$it" }
        subadmin.forEach { body = "$body&subadmin[]=$it" }
        body = "$body&quota=$quota&language=$language"
        val reply = restAPI.post(userPath)
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<AddUserResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::createUser Response Body is null")

        /* 100 - successful
         * 101 - invalid input data
         * 102 - username already exists
         * 103 - unknown error occurred whilst adding the user
         * 104 - group does not exist
         * 105 - insufficient privileges for group
         * 106 - no group specified (required for subadmins)
         * 107 - all errors that contain a hint - for example “Password is among the 1,000,000 most common ones. Please make it unique.” (this code was added in 12.0.6 & 13.0.1)
         * 108 - password and email empty. Must set password or an email
         * 109 - invitation email cannot be send */
        return reply.body.ocs.meta.statuscode == 100 ||
            reply.body.ocs.meta.statuscode == 102
    }

    private fun encodeString(value: String): String {
        return URLEncoder.encode(value, "utf-8").replace("+", "%20")
    }

    override fun getUserInfo(id: String): NextcloudUserDataEntity {
        val reply = restAPI.get("$userPath/$id").asObject<ocs<GetUserResponseEntity>>()

        reply.throwOnErrorForUser("get")
            ?: throw IllegalStateException("::getUserInfo Response Body is null")

        return reply.body.ocs.data
    }

    override fun getAllUsers(): List<String> {
        val reply = restAPI.get(userPath).asObject<ocs<GetAllUserResponseEntity>>()

        reply.throwOnErrorForUser("get")
            ?: throw IllegalStateException("::getAllUsers Response Body is null")

        return reply.body.ocs.data.users
    }

    override fun changeMailAddress(
        userid: String,
        newMailAddress: String
    ) {
        val body = "key=email&value=${encodeString(newMailAddress)}"
        val reply = restAPI.put("$userPath/$userid")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<ChangeUserResponseEntity>>()

        reply.throwOnErrorForUser("changeMailAddress")
            ?: throw IllegalStateException("::changeMailAddress Response Body is null")

        if (reply.body.ocs.meta.statuscode != 100)
            throw IllegalStateException("::changeMailAddress Failure response ${reply.body.ocs.meta.statuscode}: ${reply.body.ocs.meta.status} - ${reply.body.ocs.meta.message}")
    }

    override fun deleteUser(userID: String) {
        val reply = restAPI.delete("$userPath/$userID").asEmpty()

        reply.throwOnErrorForUser("delete")
    }

    override fun enableUser(userID: String) {
        val reply = restAPI.put("$userPath/$userID/enable").asEmpty()

        reply.throwOnErrorForUser("enable")
    }

    override fun disableUser(userID: String) {
        val reply = restAPI.put("$userPath/$userID/disable").asEmpty()

        reply.throwOnErrorForUser("disable")
    }
}

internal fun <T> HttpResponse<T>.throwOnErrorForUser(cmd: String): T? {
    when (status) {
        HttpURLConnection.HTTP_OK -> {
        }
        HttpURLConnection.HTTP_CREATED -> {
        }
        HttpURLConnection.HTTP_ACCEPTED -> {
        }
        HttpURLConnection.HTTP_NOT_ACCEPTABLE -> throw IllegalStateException("::$cmd users not accepted. ")

        HttpURLConnection.HTTP_UNAUTHORIZED -> throw IllegalStateException("::$cmd Unauthorized to read users. Bad credentials?")
        HttpURLConnection.HTTP_FORBIDDEN -> throw IllegalStateException("::$cmd Insufficient rights to $cmd users")
        HttpURLConnection.HTTP_NOT_FOUND -> throw NoSuchElementException("::$cmd users does not exist!")
        HttpURLConnection.HTTP_BAD_REQUEST -> throw IllegalStateException("::$cmd Invalid JSON content for users")
        // No constant in HttpUrlConnection for this :(
        422 -> throw IllegalArgumentException("::$cmd Requirements not met. $body")
        else -> throw IllegalStateException("::$cmd Unexpected return code $status for users")
    }
    parsingError.ifPresent {
        throw IllegalStateException(
            "::$cmd Could not parse answer from Nextcloud",
            it
        )
    }
    return body
}
