/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.asObject
import dev.maximilian.feather.nextcloud.ocs.entities.CreateShareResponseEntity
import dev.maximilian.feather.nextcloud.ocs.entities.CreateShareResponseSubEntity
import dev.maximilian.feather.nextcloud.ocs.entities.GetShareResponseEntity
import dev.maximilian.feather.nextcloud.ocs.entities.UpdateShareResponseEntity
import dev.maximilian.feather.nextcloud.ocs.entities.general.ShareDetailEntity
import dev.maximilian.feather.nextcloud.ocs.entities.general.ocs
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import kong.unirest.HttpResponse
import kong.unirest.UnirestInstance
import java.net.HttpURLConnection
import java.net.URLEncoder

internal class ShareAPI(baseURL: String, private val restAPI: UnirestInstance) : IShareAPI {
    private val basePath = "ocs/v2.php/apps/files_sharing/api/v1"
    private val sharePath = "$baseURL/$basePath/shares"

    override fun createShare(
        path: String,
        type: ShareType,
        userOrGroupIDtoShareWith: String,
        publicUpload: Boolean,
        permissions: PermissionType,
        expireDate: String?
    ): CreateShareResponseSubEntity {
        val encodedPath = encodePath(path)
        var body = "path=$encodedPath&shareType=${type.q}&shareWith=$userOrGroupIDtoShareWith&permissions=${permissions.intValue}&publicUpload=$publicUpload"
        if (expireDate != null) {
            // TOOD: test format is "YYYY-MM-dd"
            body = "$body&expireDate=$expireDate"
        }
        val reply = restAPI.post(sharePath)
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<CreateShareResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::createShare Response Body is null")

        return CreateShareResponseSubEntity.fromRaw(reply.body.ocs.data)
    }

    override fun updateShare(
        shareID: Int,
        publicUpload: Boolean,
        permissions: PermissionType,
        expireDate: String?
    ): ShareDetailEntity {
        var body = "permissions=${permissions.intValue}&publicUpload=$publicUpload"
        if (expireDate != null) {
            body = "$body&expireDate=$expireDate"
        }
        val reply = restAPI.put("$sharePath/$shareID")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<UpdateShareResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::createShare Response Body is null")

        return ShareDetailEntity.fromRaw(reply.body.ocs.data)
    }

    override fun deleteShare(shareID: Int) {
        restAPI.delete("$sharePath/$shareID").asEmpty()
    }

    override fun getSpecificShareEntity(shareID: Int): ShareDetailEntity {
        val reply = restAPI.get("$sharePath/$shareID").asObject<ocs<GetShareResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::getSpecificShareEntity Response Body is null")

        return ShareDetailEntity.fromRaw(reply.body.ocs.data.first())
    }

    override fun getAllShares(): List<ShareDetailEntity> {
        val reply = restAPI.get(sharePath).asObject<ocs<GetShareResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::getAllShares Response Body is null")

        return reply.body.ocs.data.map { ShareDetailEntity.fromRaw(it) }
    }

    private fun encodePath(path: String): String =
        path.split("/").joinToString("/") { URLEncoder.encode(it, "utf-8").replace("+", "%20") }
}

internal fun <T> HttpResponse<T>.throwOnErrorForShare(): T? {
    val cmd = "post"
    when (status) {
        HttpURLConnection.HTTP_OK -> {
        }
        HttpURLConnection.HTTP_CREATED -> {
        }
        HttpURLConnection.HTTP_ACCEPTED -> {
        }
        HttpURLConnection.HTTP_NOT_ACCEPTABLE -> throw IllegalStateException("::$cmd get shares not accepted. ")

        HttpURLConnection.HTTP_UNAUTHORIZED -> throw IllegalStateException("::$cmd Unauthorized to read shares. Bad credentials?")
        HttpURLConnection.HTTP_FORBIDDEN -> throw IllegalStateException("::$cmd Insufficient rights to $cmd shares")
        HttpURLConnection.HTTP_NOT_FOUND -> throw IllegalStateException("::$cmd File does not exist!")
        HttpURLConnection.HTTP_BAD_REQUEST -> throw IllegalStateException("::$cmd Invalid JSON content for shares")
        // No constant in HttpUrlConnection for this :(
        422 -> throw IllegalArgumentException("::$cmd Requirements not met. $body")
        else -> throw IllegalStateException("::$cmd Unexpected return code $status for shares")
    }
    parsingError.ifPresent {
        throw IllegalStateException(
            "::$cmd Could not parse answer from Nextcloud",
            it
        )
    }
    return body
}
