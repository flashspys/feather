/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import org.gradle.api.tasks.testing.logging.TestExceptionFormat

val gitVersion: groovy.lang.Closure<String> by extra

plugins {
    kotlin("jvm") version kotlinVersion
    `java-library`

    id("com.palantir.git-version") version gitPluginVersion
}

allprojects {
    group = "dev.maximilian.feather"
    version = gitVersion()

    repositories {
        mavenCentral()
    }
}

subprojects {
    apply(plugin = "kotlin")
    apply(plugin = "java-library")

    dependencies {
        // Kotlin with stdlib and coroutines
        implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
        implementation("org.jetbrains.kotlin", "kotlin-stdlib-jdk8", kotlinVersion)
        implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", coroutinesVersion)

        // Logging framework
        implementation("org.slf4j", "slf4j-api", slf4jVersion)
        runtimeOnly("org.slf4j", "slf4j-simple", slf4jVersion)
        implementation("io.github.microutils", "kotlin-logging", microUtilsVersion)

        // Testing framework
        testImplementation("org.junit.jupiter", "junit-jupiter", junitVersion)
        testImplementation(kotlin("test-junit5"))
        testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
    }

    tasks {
        compileJava {
            sourceCompatibility = "1.8"
            targetCompatibility = "1.8"
        }
        compileKotlin {
            kotlinOptions.jvmTarget = "1.8"
        }
        compileTestKotlin {
            kotlinOptions.jvmTarget = "1.8"
        }

        test {
            useJUnitPlatform()
            testLogging {
                events("passed", "skipped", "failed")

                exceptionFormat = TestExceptionFormat.FULL
            }
        }
    }
}
