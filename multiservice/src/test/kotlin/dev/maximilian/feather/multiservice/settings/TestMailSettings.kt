/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.settings

import javax.mail.internet.InternetAddress

object TestMailSettings {
    val internetAdress = InternetAddress("generalMails@mail.de", "myTestBot")

    val forgotMailSettings = SendMailContent(
        "forgotten@mail.de",
        "No problem. Here ist your restored mail."
    )

    val changeMailSetting = ChangeMailSetting(
        "mailchanger@test.de",
        "your mail had been changed",
        "notifcation 1 of change mail",
        "notifcation 2 of change mail"
    )

    val invitationMailSettings = SendMailContent("invitation@mail.de", "you are invited to test server!!!")

    val smtp = SMTPSetting(
        "smtp.testmail.com",
        "533",
        "PASSWORD",
        "TRUE",
        "user@smpt.de",
        "topsecret",
    )

    val mailSetting = MultiServiceMailSettings(
        "mytest.de",
        smtp, internetAdress,
        forgotMailSettings,
        invitationMailSettings, changeMailSetting, "nextcloud.mytest.de"
    )
}
