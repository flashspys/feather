/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.AppBuilder
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.RedisFactory
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.authorization.CredentialAuthorizer
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.authorization.api.LoginRequest
import dev.maximilian.feather.authorization.api.SessionAnswer
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.getEnv
import dev.maximilian.feather.multiservice.mockserver.EmptyCheckUserEvent
import dev.maximilian.feather.multiservice.mockserver.GroupSyncMock
import dev.maximilian.feather.multiservice.mockserver.ServiceMock
import dev.maximilian.feather.multiservice.mockserver.SessionTerminatorMock
import dev.maximilian.feather.multiservice.mockserver.UserDeletionEventMock
import dev.maximilian.feather.multiservice.settings.MultiServiceConfig
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.multiservice.settings.TestCredentialProvider
import dev.maximilian.feather.multiservice.settings.TestMailSettings
import dev.maximilian.feather.multiservice.settings.TestUser
import kong.unirest.Config
import kong.unirest.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper
import org.jetbrains.exposed.sql.Database
import redis.clients.jedis.JedisPool
import java.sql.DriverManager
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class ApiTestUtilities {
    companion object {
        internal val db = Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:feather;DB_CLOSE_DELAY=-1") })

        private val basepathPrefix = "http://127.0.0.1"
        private val basepathSuffix = "/v1"

        internal val pool: JedisPool by lazy {
            val testSetting = mutableMapOf<String, String>()
            testSetting["redis.host"] = getEnv("TEST_REDIS_HOST", "127.0.0.1")
            kotlin.run {
                RedisFactory(testSetting).create().apply {
                    resource.use { it.ping() }
                }
            }
        }
    }

    private var credentialProvider = TestCredentialProvider.credentialProvider

    val gdprController = GdprController(db, ActionController(db), credentialProvider::getUsers)

    private val backgroundJobManager = BackgroundJobManager(pool)

    private val syncEvent = GroupSyncMock()

    internal fun setCredentialProvider(cp: ICredentialProvider) {
        credentialProvider = cp
    }

    internal val restConnection = UnirestInstance(
        Config().setObjectMapper(
            JacksonObjectMapper(
                jacksonObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                    .registerModule(JavaTimeModule())
            )
        )
            .addDefaultHeader("Accept", "application/json")
    )

    var scenario: TestScenario? = null

    fun startWithDummyService(admin: Boolean): TestScenario {
        initTestSetup()
        val userDeletionEvents = listOf(UserDeletionEventMock())
        val sessionTerminatorEvent = SessionTerminatorMock()

        val app = AppBuilder().createApp("test", "test-utilities", "http://127.0.0.1", emptySet(), 0)

        AuthorizerApi(
            app,
            AuthorizationController(
                mutableSetOf(CredentialAuthorizer("credential", credentialProvider, "http://127.0.0.1")),
                credentialProvider::getUser,
                db,
                ActionController(db)
            ),
            true
        )
        val callbackEvents =
            MultiServiceEvents(syncEvent, userDeletionEvents, emptyList(), sessionTerminatorEvent, EmptyCheckUserEvent())
        val multiserviceConfig =
            MultiServiceConfig(
                credentialProvider,
                backgroundJobManager,
                db,
                TestMailSettings.mailSetting,
                callbackEvents,
                gdprController
            )
        val ms = Multiservice(multiserviceConfig)

        val testService = ServiceMock()

        ms.services.add(testService)
        ms.startApis(app)

        scenario = TestScenario(testService, "$basepathPrefix:${app.port()}$basepathSuffix")

        if (admin)
            loginWithAdminUser()
        else
            loginWithStandardUser()
        return scenario!!
    }

    fun loginWithStandardUser() {
        val session = restConnection
            .post("${scenario!!.basePath}/authorizer/credential")
            .body(LoginRequest(TestUser.standardLogin.name, TestUser.standardLogin.password))
            .asObject(SessionAnswer::class.java)

        assertNotNull(session.body)
        assertEquals(TestUser.standardLogin.name, session.body.user.username)
    }

    fun loginWithAdminUser() {
        val session = restConnection
            .post("${scenario!!.basePath}/authorizer/credential")
            .body(LoginRequest(TestUser.adminLogin.name, TestUser.adminLogin.password))
            .asObject(SessionAnswer::class.java)

        assertNotNull(session.body)
        assertEquals(TestUser.adminLogin.name, session.body.user.username)
    }

    private fun initTestSetup() {
        credentialProvider.getGroups().forEach { credentialProvider.deleteGroup(it) }
        credentialProvider.getUsers().forEach { credentialProvider.deleteUser(it) }
        credentialProvider.createUser(TestUser.standardUser, TestUser.standardLogin.password)
        credentialProvider.createUser(TestUser.adminUser, TestUser.adminLogin.password)
    }

    data class TestScenario(
        val serviceMock: ServiceMock,
        val basePath: String
    )
}
