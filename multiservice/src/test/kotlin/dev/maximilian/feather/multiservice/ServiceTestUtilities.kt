/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import com.shopify.promises.Promise
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.internals.useroperations.CreateUser
import java.time.Instant
import java.util.UUID
import kotlin.random.Random

internal class ServiceTestUtilities(private val service: IControllableService) {
    internal fun getRandomTestUser(active: Boolean): User {
        val idVal = Random.nextInt(10000).toString()

        return User(
            idVal,
            "Harry$idVal",
            "Harry$idVal Test",
            "Harry$idVal",
            "Test",
            "harry$idVal@testmail.de",
            setOf(),
            Instant.now(),
            setOf(),
            setOf(Permission.USER),
            !active
        )
    }

    internal fun createRandomTestUser(active: Boolean): User {
        val testUser = getRandomTestUser(active)
        val creationResult = service.createUser(testUser, UUID.randomUUID())
        creationResult.whenComplete { result ->
            when (result) {
                is Promise.Result.Success -> CreateUser.logger.info("Create test user successfull.")
                is Promise.Result.Error -> {
                    CreateUser.logger.error(result.error) {
                        "Create test user failed! ${result.error}"
                        throw Exception("Test failed")
                    }
                }
            }
        }
        return testUser
    }

    fun testCreatedUserTwice(): Boolean {
        val testUser = getRandomTestUser(true)
        val creationResult = service.createUser(testUser, UUID.randomUUID())
        creationResult.whenComplete { result ->
            when (result) {
                is Promise.Result.Success -> CreateUser.logger.info("Create test user in openproject successfull.")
                is Promise.Result.Error -> {
                    CreateUser.logger.error(result.error) {
                        "Create test user failed! ${result.error}"
                    }
                }
            }
        }
        var wasError = false
        val creationResult2 = service.createUser(testUser, UUID.randomUUID())
        creationResult2.whenComplete { result ->
            when (result) {
                is Promise.Result.Success -> {
                    CreateUser.logger.info("Create test user in openproject successfull.")
                    throw Exception("FAILED")
                }
                is Promise.Result.Error -> {
                    wasError = true
                }
            }
        }
        return wasError
    }

    fun createSimpleUserTest(): Boolean {
        var success = false
        val testUser = getRandomTestUser(true)
        val creationResult = service.createUser(testUser, UUID.randomUUID())
        creationResult.whenComplete { result ->
            when (result) {
                is Promise.Result.Success -> {
                    success = true
                }
                is Promise.Result.Error -> {
                    CreateUser.logger.error(result.error) {
                        "Create test user failed! ${result.error}"
                    }
                }
            }
        }
        return success
    }
}
