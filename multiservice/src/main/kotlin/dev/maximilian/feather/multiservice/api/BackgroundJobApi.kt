/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api

import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.sessionOrNull
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.NotFoundResponse
import java.util.UUID

class BackgroundJobApi(app: Javalin, val backgroundJobManager: BackgroundJobManager) {
    init {
        app.routes {
            path("background_job") {
                path(":id") {
                    get(::getJobStatus)
                    get("results", ::getJobResults)
                }
            }
        }
    }

    private fun getJobStatus(ctx: Context) {
        val creator = ctx.sessionOrNull()?.user
        val jobId = UUID.fromString(ctx.pathParam("id"))

        val jobStatus = backgroundJobManager.getJobStatus(jobId) ?: throw NotFoundResponse("Job $jobId not found")

        if (jobStatus.permission != null) {
            if (creator == null ||
                !creator.permissions.contains(jobStatus.permission)
            ) {
                throw ForbiddenResponse()
            }
        }

        ctx.json(jobStatus)
        ctx.status(200)
    }

    private fun getJobResults(ctx: Context) {
        val creator = ctx.sessionOrNull()?.user
        val jobId = UUID.fromString(ctx.pathParam("id"))

        val jobStatus = backgroundJobManager.getJobStatus(jobId) ?: throw NotFoundResponse("Job $jobId not found")

        if (jobStatus.permission != null) {
            if (creator == null ||
                !creator.permissions.contains(jobStatus.permission)
            ) {
                throw ForbiddenResponse()
            }
        }

        if (jobStatus.completed != null) {
            val jobResultsAsString = backgroundJobManager.getJobResultsAsString(jobStatus)
                ?: throw NotFoundResponse("Results for job $jobId not found")
            ctx.result(jobResultsAsString).contentType("application/json")
            ctx.status(200)
        } else {
            throw NotFoundResponse("Job $jobId not completed yet")
        }
    }
}
