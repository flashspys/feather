/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.connectors

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.internals.ChangeMailAddress
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID

internal class ChangeMailAddressDatabaseConnector(private val db: Database) {
    init {
        transaction(db) { SchemaUtils.createMissingTablesAndColumns(ChangeMailAddressTable) }
    }

    fun getChangeMailAddress(uuid: UUID, credentialProvider: ICredentialProvider): ChangeMailAddress? = transaction(db) {
        ChangeMailAddressTable.select {
            ChangeMailAddressTable.id eq EntityID(uuid, ChangeMailAddressTable)
        }.firstOrNull()?.let {
            credentialProvider.getUser(it[ChangeMailAddressTable.userId])
                ?.let { user -> rowToChangeMailAddress(it, user) }
        }?.takeIf { it.validUntil >= Instant.now() }
    }

    private fun calculateValidUntil(): Instant {
        return Instant.now().plus(30, ChronoUnit.MINUTES)
    }

    fun createChangeMailAddress(user: User, newMail: String, credentialProvider: ICredentialProvider): ChangeMailAddress = getChangeMailAddress(
        transaction(db) {
            ChangeMailAddressTable.insertAndGetId {
                it[userId] = user.id
                it[ChangeMailAddressTable.newMail] = newMail
                it[validUntil] = calculateValidUntil()
            }.value
        },
        credentialProvider
    )!!

    fun removeChangeMailAddress(changeMailAddress: ChangeMailAddress) {
        transaction(db) { ChangeMailAddressTable.deleteWhere { ChangeMailAddressTable.id eq changeMailAddress.id } }
    }

    private fun rowToChangeMailAddress(it: ResultRow, user: User): ChangeMailAddress =
        ChangeMailAddress(
            validUntil = it[ChangeMailAddressTable.validUntil],
            newMail = it[ChangeMailAddressTable.newMail],
            user = user,
            id = it[ChangeMailAddressTable.id].value
        )

    object ChangeMailAddressTable : UUIDTable("change_mail_address") {
        val userId: Column<String> = varchar("userId", 255)
        val newMail: Column<String> = varchar("newMail", 100)
        val validUntil: Column<Instant> = timestamp("validUntil")
    }
}
