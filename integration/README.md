# Integration tests

## Test overview

- Core unit tests
  - Properties database tests - need a postgres DB
  - LDAP integration tests - need an external LDAP service
  - Multiservice tests
  - IOG Plugin tests
- Nextcloud integration tests - need an external NC container
- OpenProject tests - need an external OP container

## Gitlab CI/CD

- test:ktlint: Tests for code style
- test:hadolint: Tests for Dockerfile style
- test:openldap: Core integration tests for LDAP integration with OpenLDAP
- test:389ds: Core integration tests for LDAP integration with 389ds
- test:nextcloud: Integration tests for Nextcloud integration
- test:openproject: Integration tests for OpenProject integration

These tests wil be run for each commit. You can run one of these tests locally using

```[bash]
gitlab-runner exec docker test:XYZ
```

Note: Assumes gitlab-runner ([Link](https://docs.gitlab.com/runner/install/)) and Docker to be installed.

## Native tests

All tests that only need a DB and rely on mocked services can be run without any external docker container:

```[bash]
./integration/run_native_tests.sh
```

## LDAP tests

To run the LDAP tests in local docker containers, special docker-compose config files exist.

For integration tests with OpenLDAP, run:

```[bash]
cd ./integration/openldap_test
docker-compose up -d openldap
docker-compose up feather-unit-tests # can be run several times
```

For integration tests with 389ds, run:

```[bash]
cd ./integration/389ds_test
docker-compose up -d 389ds
docker-compose up feather-unit-tests # can be run several times
```

Cleanup after each test session using

```[bash]
docker-compose down
```

in the corresponding directory.

## OpenProject tests

You can run the tests against a local installation that you can set up using:

```[bash]
OPENPROJECT_MIGRATE_IOG=0 ./integration/openproject/local_up.sh
```

This will initialize OpenProject test ready. Note that this requires a free port 8081 and will run the JDK natively. The behaviour on which port openproject is bound to the host can be changed. For more information look into `integration/openproject/variables.sh`

Now you can repeatedly run the tests:

```[bash]
./gradlew :openproject:check
```

Afterward, clean up the containers:

```[bash]
docker rm -f feather-openproject-test
```

To run the OpenProject tests completely in local docker containers, another docker-compose config exists.

For integration tests with OpenProject, run:

```[bash]
cd ./integration/openproject
docker-compose up -d openproject
docker exec -i openproject bash < ./wait_ready.sh # wait for the container to be ready
docker exec -i openproject bash < ./migrate_db.sh # initialize certain settings
docker-compose up feather-unit-tests # can be run several times
```

For each test run, specify the test(s) to be run in the `TESTS_TO_BE_RUN` variable of the [.env file](./openproject/.env).

Cleanup after each test session using

```[bash]
docker-compose down
```

## Nextcloud tests

To run the Nextcloud tests in local docker containers, another docker-compose config exists.

For integration tests with Nextcloud, run:

```[bash]
cd ./integration/nextcloud_tests
docker-compose up -d nextcloud
docker exec -i -u www-data nextcloud bash < ./wait_ready.sh # wait for the container to be ready
docker exec -i -u www-data nextcloud bash < ./install_nc_apps.sh # initialize groupfolders app once
docker-compose up feather-unit-tests # can be run several times
```

For each test run, specify the test(s) to be run in the `TESTS_TO_BE_RUN` variable of the [.env file](./nextcloud_tests/.env).

Cleanup after each test session using

```[bash]
docker-compose down
```

## MultiService and IoG plugin tests

To run the MultiService and IoG plugin tests in local docker containers, another docker-compose config exists.

For integration tests with Nextcloud, OpenProject and 389ds as LDAP provider, run:

```[bash]
cd ./integration/multi_iog_tests

docker-compose up -d 389ds

docker-compose up -d openproject
docker exec -i openproject bash < ../openproject/wait_ready.sh # wait for the container to be ready
#docker exec -i openproject bash < ../openproject/migrate_db.sh # not needed here
docker exec -i openproject bash < ../openproject/migrate_db_roles.sh # initialize membership roles

docker-compose up -d nextcloud
docker exec -i -u www-data nextcloud bash < ../nextcloud_tests/wait_ready.sh # wait for the container to be ready
docker exec -i -u www-data nextcloud bash < ../nextcloud_tests/install_nc_apps.sh # initialize groupfolders app once

docker-compose up -d redis

docker-compose up feather-unit-tests # can be run several times
```

For each test run, specify the test(s) to be run in the `TESTS_TO_BE_RUN` variable of the [.env file](./multi_iog_tests/.env).

Cleanup after each test session using

```[bash]
docker-compose down
```
