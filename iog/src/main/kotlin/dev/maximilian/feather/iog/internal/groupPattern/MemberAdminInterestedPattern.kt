/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.groupPattern

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import java.util.Locale

internal class MemberAdminInterestedPattern(
    var credentialProvider: ICredentialProvider
) : IGroupPattern {
    companion object {
        fun concatOrIgnore(name: String, prefixAndSuffix: Pair<String?, String?>): String {
            var result = name
            val prefix: String? = prefixAndSuffix.first
            if (prefix != null) {
                result = concatPrefix(result, prefix)
            }
            val suffix: String? = prefixAndSuffix.second
            if (suffix != null) {
                result = concatSuffix(result, suffix)
            }
            return result
        }

        private fun concatPrefix(name: String, prefix: String): String {
            val dashedPrefix = "$prefix-"
            return if (name.startsWith(dashedPrefix)) {
                name
            } else {
                dashedPrefix + name
            }
        }

        private fun concatSuffix(name: String, suffix: String): String {
            val dashedSuffix = "-$suffix"
            return if (name.endsWith(dashedSuffix)) {
                name
            } else {
                name + dashedSuffix
            }
        }
    }

    val adminDescriptionPrefix = "Administratoren von "
    val memberDescriptionPrefix = "Fördermitglieder der Gruppe "
    val interestedDescriptionPrefix = "Interessierte der Gruppe "
    val mainGroupDescriptionPrefix = "Alle Mitglieder von "

    override fun create(config: CreateGroupConfig): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val interested = credentialProvider.getGroupByName(LdapNames.INTERESTED_PEOPLE)
        requireNotNull(interested) { "Cannot find group '${LdapNames.INTERESTED_PEOPLE}'" }

        val interestedGroup = credentialProvider.createGroup(createInterestedGroupBody(interested.id, config))
        val memberGroup = credentialProvider.createGroup(createMemberGroupBody(iogMembers.id, config))
        val masterGroup = credentialProvider.createGroup(createMainGroupBody(interestedGroup.id, memberGroup.id, config))
        val adminGroup = credentialProvider.createGroup(createAdminGroupBody(setOf(interestedGroup.id, memberGroup.id), config))

        CreateGroup.logger.info { "MemberAdminInterestedPattern::create Created LDAP group ${masterGroup.name} with admin group ${adminGroup.name}, interested group ${interestedGroup.name}, and member group ${memberGroup.name}" }

        return arrayOf(masterGroup, adminGroup, interestedGroup, memberGroup).toList()
    }

    override fun search(prefixedName: String): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val interested = credentialProvider.getGroupByName(LdapNames.INTERESTED_PEOPLE)
        requireNotNull(interested) { "Cannot find group '${LdapNames.INTERESTED_PEOPLE}'" }

        val interestedGroup = credentialProvider.getGroupByName(getInterestedGroupName(prefixedName))
        requireNotNull(interestedGroup) { "Cannot find group '${getInterestedGroupName(prefixedName)}'" }

        val memberGroup = credentialProvider.getGroupByName(getMemberGroupName(prefixedName))
        requireNotNull(memberGroup) { "Cannot find group '${getMemberGroupName(prefixedName)}'" }

        val rgGroup = credentialProvider.getGroupByName(prefixedName)
        requireNotNull(rgGroup) { "Cannot find group '$prefixedName'" }

        val adminGroup = credentialProvider.getGroupByName(getAdminGroupName(prefixedName))
        requireNotNull(adminGroup) { "Cannot find group '${getAdminGroupName(prefixedName)}'" }

        CreateGroup.logger.info { "MemberAdminInterestedPattern::search Found existing LDAP group ${rgGroup.name} with admin group ${adminGroup.name}, interested group ${interestedGroup.name}, and member group ${memberGroup.name}" }

        return arrayOf(rgGroup, adminGroup, interestedGroup, memberGroup).toList()
    }

    private fun createMemberGroupBody(parentID: String, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = getMemberGroupName(conf.ldapName),
            description = "$memberDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = setOf(parentID),
            userMembers = emptySet(),
            owners = emptySet(),
            groupMembers = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet()
        )
    }

    private fun createInterestedGroupBody(parentGroupID: String, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = getInterestedGroupName(conf.ldapName),
            description = "$interestedDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = setOf(parentGroupID),
            userMembers = emptySet(),
            groupMembers = emptySet(),
            owners = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet()
        )
    }

    private fun createMainGroupBody(interestedGroupID: String, memberGroupID: String, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = conf.ldapName,
            description = "$mainGroupDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = emptySet(),
            userMembers = emptySet(),
            groupMembers = setOf(interestedGroupID, memberGroupID),
            owners = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet()
        )
    }

    private fun createAdminGroupBody(ownedGroupsG: Set<String>, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = getAdminGroupName(conf.ldapName),
            description = "$adminDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = emptySet(),
            userMembers = conf.ownerIDs,
            owners = emptySet(),
            groupMembers = emptySet(),
            ownerGroups = emptySet(),
            ownedGroups = ownedGroupsG
        )
    }

    private fun getMemberGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.member_suffix
    }

    private fun getInterestedGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.interested_suffix
    }

    private fun getAdminGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.admin_suffix
    }

    override fun getMetaDescription(groupName: String): GroupMetaDescription {
        val description = estimateDescriptionForRGandProjects(groupName)
        if (groupName.endsWith(IogPluginConstants.admin_suffix)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.admin_suffix.length)
            return GroupMetaDescription(
                name = getAdminGroupName(prefixedName),
                description = "$adminDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = emptySet(),
                ownerNames = emptySet(),
                groupMemberNames = emptySet(),
                ownerGroupNames = emptySet(),
                ownedGroupNames = setOf(getInterestedGroupName(prefixedName), getMemberGroupName(prefixedName)),
                descriptionWithoutPrefix = description
            )
        } else if (groupName.endsWith(IogPluginConstants.member_suffix)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.member_suffix.length)
            return GroupMetaDescription(
                name = getMemberGroupName(prefixedName),
                description = "$memberDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = setOf(LdapNames.IOG_MEMBERS),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description
            )
        } else if (groupName.endsWith(IogPluginConstants.interested_suffix)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.interested_suffix.length)
            return GroupMetaDescription(
                name = getInterestedGroupName(prefixedName),
                description = "$interestedDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = setOf(LdapNames.INTERESTED_PEOPLE),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description
            )
        } else {
            return GroupMetaDescription(
                name = groupName,
                description = "$mainGroupDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = setOf(),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getMemberGroupName(groupName), getInterestedGroupName(groupName)),
                ownerGroupNames = setOf(),
                ownedGroupNames = setOf(),
                descriptionWithoutPrefix = description
            )
        }
    }
    override fun getAllMetaDescriptions(groupName: String): List<GroupMetaDescription> {
        val prefixedName = removeSuffixIfNecessary(groupName)
        return listOf(
            getMetaDescription(prefixedName),
            getMetaDescription(getAdminGroupName(prefixedName)),
            getMetaDescription(getInterestedGroupName(prefixedName)),
            getMetaDescription(getMemberGroupName(prefixedName))
        )
    }
    private fun removeSuffixIfNecessary(groupName: String): String {
        var prefixedName = groupName
        if (groupName.endsWith(IogPluginConstants.admin_suffix)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.admin_suffix.length)
        } else if (groupName.endsWith(IogPluginConstants.member_suffix)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.member_suffix.length)
        } else if (groupName.endsWith(IogPluginConstants.interested_suffix)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.interested_suffix.length)
        }
        return prefixedName
    }

    private fun estimateDescriptionForRGandProjects(groupName: String): String {
        val schema = IogGroupSchema(credentialProvider)

        var description = removeSuffixIfNecessary(groupName)
        val q = schema.identifyNodePatternType(description)
        if (q.second == GroupKind.REGIONAL_GROUP) { // RG-Syntax (rg-ilmenau) -> RG Ilmenau
            description = description.replace("ae", "ä").replace("ue", "ü").replace("oe", "ö").replace("sz", "ß")

            if (description.length > 3)
                description = UpperCaseForPrefix(description)

            if (description.contains("-")) // consider case: rg-paderborn-bielefeld -> RG Paderborn-Bielefeld
                description = UpperCaseForWordsAfterDash(description)
        } else if (q.second == GroupKind.PROJECT) // Project Syntax iog-ken10 --> IOG-KEN10
            description = description.uppercase(Locale.getDefault())
        else { // unknown case
            description = ""
        }
        return description
    }

    override fun isAdminGroup(groupName: String): Boolean {
        return groupName.endsWith(IogPluginConstants.admin_suffix)
    }

    override fun userMembersAllowed(groupName: String): Boolean {
        return groupName.endsWith(IogPluginConstants.admin_suffix) ||
            groupName.endsWith(IogPluginConstants.member_suffix) ||
            groupName.endsWith(IogPluginConstants.interested_suffix)
    }

    override fun delete(groupName: String) {
        credentialProvider.getGroupByName(getMemberGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(getAdminGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(getInterestedGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(groupName)?.let { credentialProvider.deleteGroup(it) }
    }

    override fun isMainGroup(groupName: String): Boolean {
        return groupName == removeSuffixIfNecessary(groupName)
    }

    private fun UpperCaseForPrefix(description: String): String = "${description.subSequence(0, 2).toString().uppercase(Locale.getDefault())} ${description[3].uppercase(Locale.getDefault())}${description.subSequence(4, description.length)}"
    private fun UpperCaseForWordsAfterDash(description: String): String {
        var q = description
        for (i in 0..description.length - 2)
            if (q[i] == '-')
                q = "${q.subSequence(0, i + 1)}${q[i + 1].uppercase(Locale.getDefault())}${q.subSequence(i + 2, q.length)}"

        return q
    }
}
