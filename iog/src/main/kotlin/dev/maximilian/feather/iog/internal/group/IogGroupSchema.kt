/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminInterestedPattern
import dev.maximilian.feather.iog.internal.groupPattern.SimpleMemberAdminPattern
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants

internal class IogGroupSchema(private val credentials: ICredentialProvider) {

    companion object {
        fun groupPrefixAndSuffix(groupKind: GroupKind): Pair<String?, String?> {
            when (groupKind) {
                GroupKind.REGIONAL_GROUP -> return Pair<String?, String?>(
                    IogPluginConstants.rg_prefix.split("-").first(),
                    null
                )
                GroupKind.COMPETENCE_GROUP -> return Pair<String?, String?>(
                    IogPluginConstants.kg_prefix.split("-").first(),
                    null
                )
                GroupKind.COMMITTEE -> return Pair<String?, String?>(
                    IogPluginConstants.as_prefix.split("-").first(),
                    null
                )
                GroupKind.BILA_GROUP -> return Pair<String?, String?>(
                    IogPluginConstants.rg_prefix.split("-").first(),
                    IogPluginConstants.bila_suffix.split("-").last()
                )
                GroupKind.PR_FR_GROUP -> return Pair<String?, String?>(
                    IogPluginConstants.rg_prefix.split("-").first(),
                    IogPluginConstants.prfr_suffix.split("-").last()
                )
                GroupKind.PROJECT -> return Pair<String?, String?>(null, null)
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED -> return Pair<String?, String?>(null, null)
                GroupKind.COLLABORATION,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET -> return Pair<String?, String?>(null, null)
                GroupKind.LDAP_ONLY -> return Pair<String?, String?>(null, null)
                /* else -> throw IllegalArgumentException("invalid group kind") */
            }
        }

        val possibleSubGroupSuffixes = listOf(
            IogPluginConstants.interested_suffix,
            IogPluginConstants.member_suffix,
            IogPluginConstants.admin_suffix
        )
        val possibleKindPrefixes = listOf(
            IogPluginConstants.rg_prefix,
            IogPluginConstants.as_prefix,
            IogPluginConstants.kg_prefix
        )
    }

    enum class PatternTypes { SimpleMemberAdmin, MemberAdminInterested, Other }

    val GroupKindPatternRelation = setOf(
        Pair(GroupKind.REGIONAL_GROUP, PatternTypes.MemberAdminInterested),
        Pair(GroupKind.BILA_GROUP, PatternTypes.SimpleMemberAdmin),
        Pair(GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED, PatternTypes.MemberAdminInterested),
        Pair(GroupKind.COLLABORATION, PatternTypes.SimpleMemberAdmin),
        Pair(GroupKind.COMMITTEE, PatternTypes.SimpleMemberAdmin),
        Pair(GroupKind.LDAP_ONLY, PatternTypes.Other),
        Pair(GroupKind.COMPETENCE_GROUP, PatternTypes.SimpleMemberAdmin),
        Pair(GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC, PatternTypes.SimpleMemberAdmin),
        Pair(GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET, PatternTypes.SimpleMemberAdmin),
        Pair(GroupKind.PROJECT, PatternTypes.MemberAdminInterested),
        Pair(GroupKind.PR_FR_GROUP, PatternTypes.SimpleMemberAdmin)
    )

    val PrefixGroupKindRelation = setOf(
        Pair(IogPluginConstants.rg_prefix, GroupKind.REGIONAL_GROUP),
        Pair(IogPluginConstants.as_prefix, GroupKind.COMPETENCE_GROUP),
        Pair(IogPluginConstants.kg_prefix, GroupKind.COMPETENCE_GROUP),
        Pair(IogPluginConstants.program_prefix, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC),
        Pair(IogPluginConstants.anker_prefix, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC),

    )

    val StandardGroupnameGroupKindRelation = setOf(
        Pair(LdapNames.IOG_MEMBERS, GroupKind.LDAP_ONLY),
        Pair(LdapNames.INTERESTED_PEOPLE, GroupKind.LDAP_ONLY),
        Pair(LdapNames.CENTRAL_OFFICE, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET),
        Pair(LdapNames.OMS, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET),
        Pair(LdapNames.ASSOCIATION_BOARD, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET),
        Pair(LdapNames.PROKO, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC),
        Pair(LdapNames.AP_SPRECHERINNEN, GroupKind.COLLABORATION)
    )

    val memberAdminPattern = MemberAdminInterestedPattern(credentials)
    val simpleAdminPattern = SimpleMemberAdminPattern(credentials)

    internal fun identifyNodePatternType(groupName: String): Pair <PatternTypes, GroupKind> {
        var prefixedName = groupName
        possibleSubGroupSuffixes.forEach { prefixedName = prefixedName.substringBefore(it) }
        val groupKind = StandardGroupnameGroupKindRelation.firstOrNull { prefixedName.startsWith(it.first) }?.second
            ?: PrefixGroupKindRelation.firstOrNull { prefixedName.startsWith(it.first) }?.second
        if (groupKind == null) {
            if (isProjectName(prefixedName))
                return Pair(PatternTypes.MemberAdminInterested, GroupKind.PROJECT)
        }
        return Pair(
            GroupKindPatternRelation.firstOrNull { it.first == groupKind }?.second
                ?: PatternTypes.Other,
            groupKind ?: GroupKind.LDAP_ONLY
        )
    }

    private fun isProjectName(prefixedName: String): Boolean {
        if (prefixedName.indexOf("-") == 3) return true
        return false
    }

    fun createSubGroups(config: CreateGroupConfig): List<Group> {
        val prefixAndSuffix = groupPrefixAndSuffix(config.groupKind)
        val prefixedName = MemberAdminInterestedPattern.concatOrIgnore(config.ldapName, prefixAndSuffix)
        val pattern = GroupKindPatternRelation.find { it.first == config.groupKind }!!.second

        when (pattern) {
            PatternTypes.SimpleMemberAdmin -> {
                return simpleAdminPattern.create(config.copy(ldapName = prefixedName))
            }
            PatternTypes.MemberAdminInterested -> {
                return memberAdminPattern.create(config.copy(ldapName = prefixedName))
            }
            PatternTypes.Other ->
                return arrayOf(credentials.createGroup(Group("", config.ldapName, config.description, null, emptySet(), emptySet(), emptySet(), emptySet(), emptySet(), emptySet()))).toList()
        }
    }

    fun searchSubGroups(ldapName: String, groupKind: GroupKind): List<Group> {
        val prefixAndSuffix = groupPrefixAndSuffix(groupKind)
        val prefixedName = MemberAdminInterestedPattern.concatOrIgnore(ldapName, prefixAndSuffix)
        val pattern: PatternTypes = GroupKindPatternRelation.find { it.first == groupKind }!!.second

        when (pattern) {
            PatternTypes.SimpleMemberAdmin -> {
                return simpleAdminPattern.search(prefixedName)
            }
            PatternTypes.MemberAdminInterested -> {
                return memberAdminPattern.search(prefixedName)
            }
            PatternTypes.Other ->
                throw Exception("LDAPGroupPatternSelector::searchSubGroups invalid mode")
        }
    }
}
