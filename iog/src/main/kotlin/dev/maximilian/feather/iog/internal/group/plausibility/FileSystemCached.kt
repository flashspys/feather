/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group.plausibility

import dev.maximilian.feather.nextcloud.INextcloud
import dev.maximilian.feather.nextcloud.Nextcloud

class FileSystemCached(private val n: Nextcloud) : INextcloud by n {
    private val listFoldersCache = HashMap<String, List<String>>()
    private val listFilesCache = HashMap<String, List<String>>()

    override fun createFolder(path: String) {
        n.createFolder(path)
        reset()
    }

    override fun listFiles(path: String): List<String> {
        if (!listFilesCache.contains(path))
            listFilesCache[path] = n.listFiles(path)
        return listFilesCache[path]!!
    }

    override fun listFolders(path: String): List<String> {
        if (!listFoldersCache.contains(path))
            listFoldersCache[path] = n.listFolders(path)
        return listFoldersCache[path]!!
    }

    override fun copy(source: String, destination: String) {
        n.copy(source, destination)
        reset()
    }

    override fun createFile(path: String, content: String) {
        n.createFile(path, content)
        reset()
    }

    override fun delete(path: String) {
        n.delete(path)
        reset()
    }

    fun reset() {
        listFoldersCache.clear()
        listFilesCache.clear()
    }
}
