/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.iog.api.AutoRepair
import dev.maximilian.feather.iog.api.GroupPlausibilityConfig
import dev.maximilian.feather.iog.internal.group.plausibility.CredentialPlausibility
import dev.maximilian.feather.iog.internal.group.plausibility.NCPlausibility
import dev.maximilian.feather.iog.internal.group.plausibility.OpenProjectPlausibility
import dev.maximilian.feather.iog.internal.group.plausibility.RepairMessages
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupResolver
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.internals.plausibility.BackgroundJobStatusHelper
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.nextcloud.Nextcloud
import mu.KLogging
import java.time.Duration
import java.time.Instant
import java.util.UUID

internal data class GroupPlausibilityEntry(
    val group: Group,
    val generalStatus: String
)

internal data class GroupPlausibilityResults(
    val entries: List<GroupPlausibilityEntry>,
    val overallStatus: String
)

internal data class GroupRepairOptions(
    val autoRepair: Boolean,
    val ncRepair: Boolean,
    val opRepair: Boolean
)

internal class GroupPlausibility(
    private val credentials: ICredentialProvider,
    val backgroundJobManager: BackgroundJobManager,
    private val iogSchema: IogGroupSchema,
    private val openProjectService: OpenProjectService,
    private val syncEvent: GroupSynchronizationEvent,
    private val onc: OPNameConfig,
    private val nc: Nextcloud,
    private val nextcloudPublicURL: String
) {
    companion object : KLogging() {
        const val MISSING: String = "LDAP-Gruppe fehlt"
        const val OK_MESSAGE: String = "OK"
        const val NOK_MESSAGE: String = "Not OK"
    }

    suspend fun checkGroupPlausibility(
        jobId: UUID,
        config: GroupPlausibilityConfig
    ): GroupPlausibilityResults {
        if (config.autoRepair == AutoRepair.RESYNC_ONLY) {
            backgroundJobManager.setJobStatus(jobId, "Resync LDAP through services")
            logger.info { "GroupPlausibility::checkGroupPlausibility Resynchronisation vor Plausibilitätsprüfung angefordert." }
            syncEvent.synchronize()
            logger.info { "GroupPlausibility::checkGroupPlausibility Resynchronisation vor Plausibilitätsprüfung durchgeführt." }
        }

        backgroundJobManager.setJobStatus(jobId, "Fetching groups")
        val userAndGroups = credentials.getUsersAndGroups()
        var opPlausibility: OpenProjectPlausibility? = null
        if (config.checkOpenProject)
            opPlausibility = OpenProjectPlausibility(openProjectService.openproject, credentials)

        var ncPlausibility: NCPlausibility? = null
        if (config.checkNextcloud)
            ncPlausibility = NCPlausibility(nc, backgroundJobManager, nextcloudPublicURL, credentials, jobId)
        val entries = checkStandardGroups(jobId, userAndGroups.second)
        var allOk = entries.isEmpty()

        val statusHelper = BackgroundJobStatusHelper(
            jobId,
            "Processing exiting groups",
            userAndGroups.second.size,
            backgroundJobManager
        )

        var currentEntry = 0
        val opc = OpenProjectPCreator(openProjectService.openproject, onc, jobId, syncEvent, backgroundJobManager)
        val credPlausibility = CredentialPlausibility(iogSchema, credentials)
        var resyncRequired = false

        userAndGroups.second.forEach { group ->
            logger.debug { "GroupPlausibility::checkGroupPlausibility check Group ${group.name}" }
            val repairGroup = getRepairOptionsForGroup(config, group.id)

            val ldapStatus = credPlausibility.checkLDAPStatus(group, userAndGroups.first, userAndGroups.second, repairGroup.autoRepair)

            if (ldapStatus.pattern?.isMainGroup(group.name) == true) {
                val missingCheckResult = checkForMissingGroups(
                    userAndGroups,
                    ldapStatus.pattern.getAllMetaDescriptions(group.name)
                )
                entries.addAll(missingCheckResult.first)
                allOk = allOk && missingCheckResult.second
            }

            val errorMessages = ldapStatus.errorMessage
            if (config.checkOpenProject && ldapStatus.pattern != null) {
                val checkResult = opPlausibility!!.check(
                    group,
                    ldapStatus.patternType,
                    ldapStatus.pattern,
                    opc,
                    userAndGroups.first,
                    repairGroup.autoRepair
                )
                errorMessages.addAll(checkResult.errorMessages)
                if (checkResult.resync) resyncRequired = true
            }
            if (config.checkNextcloud && ldapStatus.pattern != null && !group.name.startsWith(LdapNames.PROKO))
                errorMessages.addAll(
                    ncPlausibility!!.check(
                        group, ldapStatus.patternType, ldapStatus.pattern, openProjectService.openproject,
                        repairGroup.autoRepair
                    )
                )
            val generalStatus = if (errorMessages.isEmpty()) OK_MESSAGE else errorMessages.joinToString(" ")
            if (generalStatus != OK_MESSAGE) allOk = false

            entries.add(GroupPlausibilityEntry(group, generalStatus))
            if (generalStatus == OK_MESSAGE)
                logger.debug { "GroupPlausibility::checkGroupPlausibility ${group.name} completed. General Status= $generalStatus" }
            else
                logger.warn { "GroupPlausibility::checkGroupPlausibility ${group.name} completed. General Status= $generalStatus" }
            currentEntry += 1
            statusHelper.updateStatus(currentEntry)
        }

        if (resyncRequired) {
            logger.info { "GroupPlausibility::checkGroupPlausibility Resynchronisation durch Autoreperatur angefordert." }
            syncEvent.synchronize()
            logger.info { "GroupPlausibility::checkGroupPlausibility Resynchronisation durchgeführt." }
        }

        return GroupPlausibilityResults(
            entries,
            when (allOk) {
                true -> OK_MESSAGE
                else -> NOK_MESSAGE
            }
        )
    }

    private fun checkStandardGroups(jobId: UUID, groups: Collection<Group>): MutableList<GroupPlausibilityEntry> {
        val startTime = Instant.now()
        val entries = mutableListOf<GroupPlausibilityEntry>()
        iogSchema.StandardGroupnameGroupKindRelation.forEach {
            val pattern = iogSchema.identifyNodePatternType(it.first)
            var necessaryGroups: List<GroupMetaDescription>? = null
            if (pattern.first == IogGroupSchema.PatternTypes.MemberAdminInterested) {
                necessaryGroups = iogSchema.memberAdminPattern.getAllMetaDescriptions(it.first)
            } else if (pattern.first == IogGroupSchema.PatternTypes.SimpleMemberAdmin) {
                necessaryGroups = iogSchema.simpleAdminPattern.getAllMetaDescriptions(it.first)
            }

            if (necessaryGroups != null) {
                val statusHelper = BackgroundJobStatusHelper(
                    jobId,
                    "Processing expected groups",
                    necessaryGroups.size,
                    backgroundJobManager
                )

                var currentEntry = 0

                necessaryGroups.forEach { necessaryGroup ->
                    if (!groups.any { it.name == necessaryGroup.name }) {
                        entries.add(
                            GroupPlausibilityEntry(
                                Group(
                                    "", necessaryGroup.name, necessaryGroup.description, necessaryGroup.dnParent,
                                    emptySet(), emptySet(), emptySet(), emptySet(), emptySet(), emptySet()
                                ),
                                "$MISSING ${RepairMessages.MANUAL}."
                            )
                        )
                        logger.warn { "GroupPlausibility::checkStandardGroups missing group ${necessaryGroup.name} detected." }
                    }

                    currentEntry += 1
                    statusHelper.updateStatus(currentEntry)
                }
            }
        }

        logger.info {
            "GroupPlausibility::checkStandardGroups finished check for ${groups.count()} groups in ${
            Duration.between(
                startTime,
                Instant.now()
            ).toMillis()
            } ms."
        }
        return entries
    }

    private fun checkForMissingGroups(
        userAndGroups: Pair<Collection<User>, Collection<Group>>,
        metaDescription: List<GroupMetaDescription>
    ): Pair<List<GroupPlausibilityEntry>, Boolean> {
        var allOK = true
        val entries = mutableListOf<GroupPlausibilityEntry>()
        metaDescription.forEach {
            if (credentials.getGroupByName(it.name) == null) {
                logger.debug { "GroupPlausibility::checkForMissingGroups failed to find ${it.name}" }
                val g = GroupResolver(userAndGroups.first, userAndGroups.second).getGroup(it, credentials)
                entries.add(GroupPlausibilityEntry(g, "$MISSING ${RepairMessages.MANUAL}."))
                allOK = false
            }
        }
        return Pair(entries, allOK)
    }

    private fun getRepairOptionsForGroup(config: GroupPlausibilityConfig, thisGroupID: String): GroupRepairOptions =
        GroupRepairOptions(
            (config.autoRepair == AutoRepair.AUTOREPAIR_ALL) || (config.autoRepair == AutoRepair.AUTOREPAIR_SELECTED && config.autoRepairGroupIds != null && config.autoRepairGroupIds.contains(thisGroupID)),
            config.checkNextcloud,
            config.checkOpenProject
        )
}
