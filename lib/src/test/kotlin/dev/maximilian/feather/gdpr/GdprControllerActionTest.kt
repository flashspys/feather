package dev.maximilian.feather.gdpr

import dev.maximilian.feather.Permission
import dev.maximilian.feather.UserRequestInfo
import dev.maximilian.feather.action.ActionController
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.Database
import java.sql.DriverManager
import java.time.Instant
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals

class GdprControllerActionTest {
    private val users = (0 until 6).map { generateTestUser(permissions = if (it == 0) setOf(Permission.MANAGE_GDPR) else emptySet()) }
    private fun initDatabase(name: String = UUID.randomUUID().toString()): Database = Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$name;DB_CLOSE_DELAY=-1") })

    @Test
    fun `Test empty gdpr database creates no actions`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        GdprController(db, actionController) { users }

        assertEquals(emptyList(), actionController.getOpenUserActions(GdprAction.NAME))
    }

    @Test
    fun `Test gdpr database contains a latest document which no one accepted creates actions for all users`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val gdprDatabase = GdprDatabase(db)

        val document = gdprDatabase.createDocument(generateTestGdprDocument(forceDate = Instant.now().plusMillis(25)))

        runBlocking { delay(50) }

        GdprController(db, actionController) { users }

        assertEquals(users.map { GdprAction(it.id, document.id) }, actionController.getOpenUserActions(GdprAction.NAME))
    }

    @Test
    fun `Test gdpr database contains a latest document which no one accepted creates no actions for all users because it is not in force state`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val gdprDatabase = GdprDatabase(db)

        gdprDatabase.createDocument(generateTestGdprDocument())

        runBlocking { delay(50) }

        GdprController(db, actionController) { users }

        assertEquals(emptyList(), actionController.getOpenUserActions(GdprAction.NAME))
    }

    @Test
    fun `Test gdpr database contains a latest document which some users accepted creates actions for the other users`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val gdprDatabase = GdprDatabase(db)

        val document = gdprDatabase.createDocument(generateTestGdprDocument(forceDate = Instant.now().plusMillis(25)))
        runBlocking { delay(50) }

        gdprDatabase.storeAcceptanceInfo(document, users[0])
        gdprDatabase.storeAcceptanceInfo(document, users[2])

        GdprController(db, actionController) { users }

        assertEquals(listOf(users[1], users[3], users[4], users[5]).map { GdprAction(it.id, document.id) }, actionController.getOpenUserActions(GdprAction.NAME))
    }

    @Test
    fun `Test gdpr database contains a latest document which some users accepted and some users already have an action for creates actions for the remaining users`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val gdprDatabase = GdprDatabase(db)

        val document = gdprDatabase.createDocument(generateTestGdprDocument(forceDate = Instant.now().plusMillis(25)))
        runBlocking { delay(50) }

        gdprDatabase.storeAcceptanceInfo(document, users[0])
        gdprDatabase.storeAcceptanceInfo(document, users[2])

        val actionsExisting = listOf(GdprAction(users[3].id, document.id), GdprAction(users[5].id, document.id))

        actionsExisting.forEach { actionController.insertAction(it) }

        GdprController(db, actionController) { users }

        assertEquals((setOf(users[1], users[4]).map { GdprAction(it.id, document.id) } + actionsExisting).toSet(), actionController.getOpenUserActions(GdprAction.NAME).toSet())
    }

    @Test
    fun `Test gdpr database contains a latest document which no one accepted and some users already have an action for creates actions for the remaining users`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val gdprDatabase = GdprDatabase(db)

        val document = gdprDatabase.createDocument(generateTestGdprDocument(forceDate = Instant.now().plusMillis(25)))
        runBlocking { delay(50) }

        val actionsExisting = listOf(GdprAction(users[3].id, document.id), GdprAction(users[5].id, document.id))

        actionsExisting.forEach { actionController.insertAction(it) }

        GdprController(db, actionController) { users }

        assertEquals((setOf(users[0], users[1], users[2], users[4]).map { GdprAction(it.id, document.id) } + actionsExisting).toSet(), actionController.getOpenUserActions(GdprAction.NAME).toSet())
    }

    @Test
    fun `A new gdpr document removes all existing gdpr actions`() {
        val db = initDatabase()
        val actionController = ActionController(db)

        val gdprDatabase = GdprDatabase(db)
        gdprDatabase.createDocument(generateTestGdprDocument(validDate = Instant.now().plusMillis(25), forceDate = Instant.now().plusMillis(15)))
        runBlocking { delay(50) }

        val gdprController = GdprController(db, actionController) { users }
        gdprController.createDocument(UserRequestInfo(users[0], false), generateTestGdprDocument())

        // Document is not forced yet
        assertEquals(emptyList(), actionController.getOpenUserActions(GdprAction.NAME))
    }

    @Test
    fun `Accepting a gdpr document removes the corresponding action from the user`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val gdprDatabase = GdprDatabase(db)
        val document = gdprDatabase.createDocument(generateTestGdprDocument(forceDate = Instant.now().plusMillis(25)))
        runBlocking { delay(50) }

        val gdprController = GdprController(db, actionController) { users }
        gdprController.storeAcceptanceInfo(UserRequestInfo(users[1], true), document.id)

        assertEquals((users - users[1]).map { GdprAction(it.id, document.id) }, actionController.getOpenUserActions(GdprAction.NAME))
    }

    @Test
    fun `Deleting a user removes the corresponding user gdpr action`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val mutableUsers = users.toMutableList()

        val gdprDatabase = GdprDatabase(db)
        val document = gdprDatabase.createDocument(generateTestGdprDocument(forceDate = Instant.now().plusMillis(25)))
        runBlocking { delay(50) }

        val gdprController = GdprController(db, actionController, mutableUsers::asIterable)
        val user = users[1]

        mutableUsers.remove(user)
        gdprController.handleUserDeleted(user)

        assertEquals(mutableUsers.map { GdprAction(it.id, document.id) }, actionController.getOpenUserActions(GdprAction.NAME))
    }

    @Test
    fun `Creating a user adds the corresponding user gdpr action`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val mutableUsers = users.toMutableList()

        val gdprDatabase = GdprDatabase(db)
        val document = gdprDatabase.createDocument(generateTestGdprDocument(forceDate = Instant.now().plusMillis(25)))
        runBlocking { delay(50) }

        val gdprController = GdprController(db, actionController, mutableUsers::asIterable)
        val user = generateTestUser()

        mutableUsers.add(user)
        gdprController.handleUserCreated(user)

        assertEquals(mutableUsers.map { GdprAction(it.id, document.id) }, actionController.getOpenUserActions(GdprAction.NAME))
    }

    @Test
    fun `Creating a user adds the corresponding user gdpr action only when user not already accepted`() {
        val db = initDatabase()
        val actionController = ActionController(db)
        val mutableUsers = users.toMutableList()

        val gdprDatabase = GdprDatabase(db)
        val document = gdprDatabase.createDocument(generateTestGdprDocument(forceDate = Instant.now().plusMillis(25)))
        runBlocking { delay(50) }

        val gdprController = GdprController(db, actionController, mutableUsers::asIterable)
        val user = generateTestUser()

        mutableUsers.add(user)

        gdprController.storeAcceptanceInfo(UserRequestInfo(user, true), document.id)
        gdprController.handleUserCreated(user)

        assertEquals((mutableUsers - user).map { GdprAction(it.id, document.id) }, actionController.getOpenUserActions(GdprAction.NAME))
    }
}
