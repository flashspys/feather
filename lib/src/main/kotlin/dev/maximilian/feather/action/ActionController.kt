package dev.maximilian.feather.action

import dev.maximilian.feather.User
import org.jetbrains.exposed.sql.Database

public class ActionController(database: Database) {
    private val actionProducerMap = mutableMapOf<String, (String, String) -> Action<*>>()
    private val actionDatabase = ActionDatabase(database)

    internal fun registerActionType(type: String, producer: (String, String) -> Action<*>) {
        actionProducerMap[type] = producer
    }

    private fun constructAction(type: String, userId: String, payload: String): Action<*> {
        val producer = requireNotNull(actionProducerMap[type]) {
            "Action \"$type\" was not registered to ActionController and cannot be constructed"
        }

        return producer(userId, payload)
    }

    public fun getOpenUserActions(fromActionType: String): List<Action<*>> =
        actionDatabase.getActionsByName(fromActionType, ::constructAction)

    public fun getOpenUserActions(fromUser: User): List<Action<*>> =
        actionDatabase.getActionsByUser(fromUser.id, ::constructAction)

    internal fun insertAction(action: Action<*>) {
        actionDatabase.insertAction(action)
    }

    internal fun deleteActionsByName(name: String) {
        actionDatabase.deleteActionsByName(name)
    }

    internal fun deleteActionsByNameAndUser(name: String, user: User) {
        actionDatabase.deleteActionsByNameAndUser(name, user.id)
    }
}
