/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.IAuthorizer
import dev.maximilian.feather.Session
import dev.maximilian.feather.User
import dev.maximilian.feather.action.ActionController
import org.jetbrains.exposed.sql.Database
import java.util.UUID

public class AuthorizationController(
    public val authorizer: MutableSet<IAuthorizer>,
    userFromIdFunction: (String) -> User?,
    database: Database,
    private val actionController: ActionController
) {
    private val sessionDatabase by lazy { SessionDatabase(database, userFromIdFunction, authorizer.associateBy { it.id }) }

    public fun getFinalizedRedirectUrl(session: Session): String {
        val result = sessionDatabase.getAfterActionsRedirect(session) ?: "/"
        val modifiedSession = session.authorizer.afterActionsSessionModification(session)

        if (modifiedSession == null) {
            sessionDatabase.deleteSession(session)
        } else {
            sessionDatabase.updateSession(modifiedSession)
        }

        return result
    }

    public fun getSession(id: UUID): Session? {
        val sessionInDatabase = sessionDatabase.getSession(id) ?: return null
        val refreshedSession = sessionInDatabase.authorizer.refresh(sessionInDatabase) ?: return null
        // Sessions can drop to minisessions, but can only be recovered by the finalizing action of the actions endpoint
        val miniSession = refreshedSession.miniSession || actionController.getOpenUserActions(refreshedSession.user).isNotEmpty()
        return sessionDatabase.updateSession(refreshedSession.copy(miniSession = miniSession))
    }

    public fun finalizeAuth(session: Session): Session {
        val finalizedSession = sessionDatabase.create(session.copy(miniSession = actionController.getOpenUserActions(session.user).isNotEmpty()))

        if (finalizedSession.miniSession) {
            sessionDatabase.insertAfterActionsRedirect(finalizedSession, session.authorizer.afterActionsRedirect(finalizedSession))
        }

        return finalizedSession
    }

    public fun deleteSessionsForUser(userId: String): Unit = sessionDatabase.deleteSessionsForUser(userId)

    public fun deleteSession(session: Session): Unit = sessionDatabase.deleteSession(session)
}
