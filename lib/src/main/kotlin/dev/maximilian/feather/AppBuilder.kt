package dev.maximilian.feather

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.javalin.Javalin
import io.javalin.core.validation.JavalinValidation
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.HttpResponseExceptionMapper
import io.javalin.http.UnauthorizedResponse
import io.javalin.plugin.json.JavalinJackson
import io.javalin.plugin.openapi.OpenApiOptions
import io.javalin.plugin.openapi.OpenApiPlugin
import io.javalin.plugin.openapi.ui.SwaggerOptions
import io.swagger.v3.oas.models.info.Info
import java.util.UUID

public class AppBuilder {
    private companion object {
        init {
            JavalinValidation.register(UUID::class.java) { UUID.fromString(it) }
            JavalinJackson.configure(
                jacksonObjectMapper().registerModule(JavaTimeModule())
                    .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            )
        }
    }

    public fun createApp(
        versionString: String,
        titleString: String,
        frontendBaseUrl: String,
        additionalAllowedOrigins: Set<String> = emptySet(),
        port: Int = 7000
    ): Javalin = Javalin.create {
        it.showJavalinBanner = false
        it.logIfServerNotStarted = false
        it.defaultContentType = "application/json"
        it.contextPath = "/v1"
        it.registerPlugin(
            OpenApiPlugin(
                OpenApiOptions(
                    Info().apply {
                        version = versionString
                        title = titleString
                    }
                )
                    .path("/openapi")
                    .swagger(SwaggerOptions("/swagger"))
            )
        )

        if (additionalAllowedOrigins.contains("*")) {
            it.enableCorsForAllOrigins()
        } else {
            it.enableCorsForOrigin(*(setOf(frontendBaseUrl) + additionalAllowedOrigins).toTypedArray())
        }

        it.accessManager { handler, ctx, permittedRoles ->
            if (permittedRoles.isEmpty() || ctx.session().user.permissions.intersect(permittedRoles).isNotEmpty()) {
                handler.handle(ctx)
            } else {
                throw PermissionException(
                    ctx.session().user,
                    "${ctx.method()} ${ctx.path()}",
                    permittedRoles.mapNotNull { r -> r as? Permission }.toSet()
                )
            }
        }
    }.start(port).apply {
        exception(PermissionException::class.java) { ex, ctx ->
            if (ex.unauthorized) {
                HttpResponseExceptionMapper.handle(UnauthorizedResponse(ex.message!!), ctx)
            } else {
                HttpResponseExceptionMapper.handle(ForbiddenResponse(ex.message!!), ctx)
            }
        }

        exception(MiniSessionException::class.java) { ex, ctx ->
            HttpResponseExceptionMapper.handle(ForbiddenResponse(ex.message!!), ctx)
        }

        exception(IllegalArgumentException::class.java) { ex, ctx ->
            HttpResponseExceptionMapper.handle(BadRequestResponse(ex.message ?: "Bad request"), ctx)
        }
    }
}

public fun Context.sessionOrMinisessionOrNull(): Session? = this.attribute<Session>("session")
public fun Context.sessionOrMinisession(): Session = this.sessionOrMinisessionOrNull() ?: throw UnauthorizedResponse()
public fun Context.session(): Session = sessionOrMinisessionOrNull()?.let { if (it.miniSession) throw ForbiddenResponse("User has only a miniSession but a full session is required") else it } ?: throw UnauthorizedResponse()
public fun Context.sessionOrNull(): Session? = sessionOrMinisessionOrNull()?.let { if (it.miniSession) null else it }
