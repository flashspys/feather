package dev.maximilian.feather.gdpr

import java.time.Instant

public data class GdprAcceptance(
    val userId: String,
    val timestamp: Instant,
    val document: GdprDocument
)
